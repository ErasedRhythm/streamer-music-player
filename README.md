# Streamer music player

This music player is made for Twitch Streamer. It gives the possibility to add local files (for the extra bit of quality) and also YouTube Videos to your playlist.

Let your viewers request their own songs to play, make easy polls, let your viewers know who your streaming with and let your viewers have fun with your chat currency.

Installation
------------

Get the newest version by navigating to build/libs and selecting the newest version.

Start the software by navigating with your command line and starting it with `java -jar`.

The Player needs some configuration to start. After that you are ready to go!

package de.stronnetwork;

import de.stronnetwork.utilities.client.SimpleClient;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Database {
    private static Database db;
    private Connection dbcon;

    private Database() {
        try {
            File dbFile = new File("data.sqlitedb");
            String path = "jdbc:sqlite:" + dbFile.getAbsolutePath();
            dbcon = DriverManager.getConnection(path);
            createTables();
            dbFile.createNewFile();

        } catch (SQLException | IOException ex) {
            System.err.println("DBConnection failed: " + ex.getMessage());
        }
    }

    private synchronized void createTables() {
        simpleSQLStatement("CREATE TABLE IF NOT EXISTS Client ("
                + "	name		varchar(64)     PRIMARY KEY NOT NULL		,"
                + "	money   	INTEGER		    NOT NULL					,"
                + " watchtime	INTEGER   	    NOT NULL    DEFAULT 0		)");
        simpleSQLStatement("CREATE TABLE IF NOT EXISTS Song ("
                + "	file		varchar(256) PRIMARY KEY    NOT NULL		,"
                + "	likes		INTEGER		                NOT NULL        DEFAULT 0		);");
    }

    private void simpleSQLStatement(String sql) {
        try {
            Statement statement = dbcon.createStatement();
            statement.execute(sql);
        } catch (SQLException e) {
            System.err.println("Could not Create Table.");
            e.printStackTrace();
        }
    }

    public static Database getDatabase() {
        if (db == null)
            db = new Database();
        return db;
    }

    public int getSongRating(String string) {
        try {
            String sql = "SELECT likes FROM Song where file = ?";
            PreparedStatement prep = dbcon.prepareStatement(sql);
            prep.setString(1, string);
            ResultSet result = prep.executeQuery();
            if (result.isClosed())
                return -1;
            else {
                int likes = result.getInt("likes");
                result.close();
                return likes;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void addSong(String song) {
        try {
            String sql = "INSERT OR IGNORE INTO Song (file) VALUES (?)";
            PreparedStatement statement = dbcon.prepareStatement(sql);
            statement.setString(1, song);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void updateSongRating(String currentSong, int newVal) {
        String sql = "UPDATE Song SET likes = ? WHERE file = ?";
        try {
            PreparedStatement statement = dbcon.prepareStatement(sql);
            statement.setInt(1, newVal);
            statement.setString(2, currentSong);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getMoney(String client) {
        try {
            String sql = "SELECT money FROM Client where name = ?";
            PreparedStatement prep = dbcon.prepareStatement(sql);
            prep.setString(1, client);
            ResultSet result = prep.executeQuery();
            if (result.isClosed())
                return -1;
            else {
                int money = result.getInt("money");
                result.close();
                return money;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public synchronized void addClient(String client) {
        try {
            String sql = "INSERT OR IGNORE INTO Client (name, money) VALUES (?, 0)";
            PreparedStatement statement = dbcon.prepareStatement(sql);
            statement.setString(1, client);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void addMoney(String client, int additionalMoney) {
        if (client.isEmpty()) {
            System.err.println("client name can not be empty");
            return;
        }
        if (additionalMoney == 0)
            return;
        addClient(client);
        String sql = "UPDATE Client SET money = ? WHERE name = ?";
        try {
            PreparedStatement statement = dbcon.prepareStatement(sql);
            statement.setInt(1, getMoney(client) + additionalMoney);
            statement.setString(2, client);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addTime(String client, int minutes) {
        if (client.isEmpty()) {
            System.err.println("client name can not be empty");
            return;
        }
        addClient(client);
        String sql = "UPDATE Client SET watchtime = ? WHERE name = ?";
        try {
            PreparedStatement statement = dbcon.prepareStatement(sql);
            statement.setInt(1, getOnlineTime(client) + minutes);
            statement.setString(2, client);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int getOnlineTime(String client) {
        try {
            String sql = "SELECT watchtime FROM Client where name = ?";
            PreparedStatement prep = dbcon.prepareStatement(sql);
            prep.setString(1, client);
            ResultSet result = prep.executeQuery();
            if (result.isClosed())
                return -1;
            else {
                int onlinetime = result.getInt("watchtime");
                result.close();
                return onlinetime;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public List<Map.Entry<Integer, SimpleClient>> getTopMoneyClients() {
        List<Map.Entry<Integer, SimpleClient>> topList = new ArrayList<>();
        try {
            String sql = "SELECT name, money FROM Client ORDER BY money DESC LIMIT 10;";
            ResultSet result = dbcon.prepareStatement(sql).executeQuery();
            while (result.next()) {
                int money = result.getInt("money");
                SimpleClient client = new SimpleClient(result.getString("name"));
                topList.add(new AbstractMap.SimpleEntry<>(money, client));
            }
            return topList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean clientExists(String client) {
        try {
            String sql = "SELECT * FROM Client where name = ?";
            PreparedStatement prep = dbcon.prepareStatement(sql);
            prep.setString(1, client);
            ResultSet result = prep.executeQuery();
            return !result.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }
}

package de.stronnetwork.GUI.FirstStartUpWindow;

import de.stronnetwork.MainApplication;
import de.stronnetwork.Settings;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * A controller that prompts the user to input important Settings before the main program starts.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.1
 * @since 0.5
 */
public class StartUpController {
    public TextField authNickname;
    public PasswordField authKey;
    public TextField authChannel;


    /**
     * This method saves all fields. When an field is empty this will return without saving.
     * When the saving is successful the main program will get initiated.
     */
    public void saveSettings() {
        Settings settings = Settings.getSettings();
        String nickname = authNickname.textProperty().getValue();
        String key = authKey.textProperty().getValueSafe();
        String channel = authChannel.textProperty().getValueSafe().toLowerCase();
        if (nickname.isEmpty() || key.isEmpty() || channel.isEmpty()) return;
        settings.setAuthNickname(nickname);
        settings.setAuthKey(key);
        settings.setAuthChannel('#' + channel);


        MainApplication.startup();
    }

    /**
     * This method opens the a <a href="https://twitchapps.com/tmi/">website</a> for the user to get their OAuth-key
     * using the standard external browser.
     */
    public void openOAuthInBrowser() {
        try {
            Desktop.getDesktop().browse(new URI("https://twitchapps.com/tmi/"));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}

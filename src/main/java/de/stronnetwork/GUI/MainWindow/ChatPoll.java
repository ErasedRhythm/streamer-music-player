package de.stronnetwork.GUI.MainWindow;

import de.stronnetwork.utilities.twitch.TwitchUtility;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.Arrays;

public class ChatPoll {

    public TextField pollQuestion;
    public TextField pollAnswer1;
    public TextField pollAnswer2;
    public TextField pollAnswer3;
    public TextField pollAnswer4;
    public TextField pollAnswer5;
    public TextField pollAnswer6;
    public TextField pollDurationField;

    private MainController parentController;
    private TwitchUtility twitchUtility;

    public void initialize() {
        twitchUtility = new TwitchUtility();
    }

    public void setParentController(MainController parentController) {
        this.parentController = parentController;
    }

    public void startPoll() {
        TwitchUtility twitchUtility = getTwitchUtility();
        try {
            String question = pollQuestion.textProperty().getValue();
            ArrayList<String> answers = new ArrayList<>();

            for (TextField field : Arrays.asList(pollAnswer1, pollAnswer2, pollAnswer3, pollAnswer4, pollAnswer5, pollAnswer6)) {
                String answer = field.textProperty().getValue();
                if (!answer.isEmpty() && !answers.contains(answer)) {
                    answers.add(answer);
                }
            }

            int duration = Integer.parseInt(pollDurationField.textProperty().getValue());

            twitchUtility.startPoll(parentController.pollResultsBarChart, question, answers, duration);
        } catch (NumberFormatException ignore) {}
    }

    private TwitchUtility getTwitchUtility() {
        return twitchUtility;
    }
}

package de.stronnetwork.GUI.MainWindow;

import de.stronnetwork.Player;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * The Controller for the Main Window of the Player Application.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.6
 */
public class MainController {

    /**
     * The Controller for the Creation of the Polls. See also {@link ChatPoll here} for more information.
     */
    public ChatPoll ChatPollController;

    /**
     * The Controller for the BarChart. Will be filled with the results of the Chat Polls.
     */
    public BarChart pollResultsBarChart;

    /**
     * Opens a new Window with the Settings for the Music Player.
     */
    public void openSettings() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/de/stronnetwork/GUI/SettingsWindow/SettingsWindow.fxml"));
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The initialization is called directly after the Constructor. Initialises every Controller and Node.
     */
    public void initialize() {
        ChatPollController.setParentController(this);
    }

    /**
     * Quits the Program on method Call.
     */
    public void onQuit() {
        if (Player.getInstance() != null)
            Player.getInstance().pause();
        Platform.exit();
        System.exit(0);
    }

    /**
     * Opens a new Window with an About Info of this Program.
     */
    public void onAbout() {
        //TODO Make About Window 
    }
}

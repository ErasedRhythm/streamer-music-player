package de.stronnetwork.GUI.MainWindow;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

import java.io.ByteArrayInputStream;

public class MediaBox {
    public ImageView albumImage;
    public MediaView mediaView;

    private boolean mediaViewVisibility = false;
    private boolean currentlyWithVideo = false;

    public void setMediaViewVisibility(boolean visibility) {
        mediaViewVisibility = visibility;
        setMediaViewManaged(mediaViewVisibility ? currentlyWithVideo : false);
    }

    private void setMediaViewManaged(boolean managed) {
        mediaView.setVisible(managed);
        mediaView.setManaged(managed);
        albumImage.setVisible(!managed);
        albumImage.setManaged(!managed);
    }

    public void newSongInfo(boolean video, byte[] albumImg) {
        currentlyWithVideo = video;
        setMediaViewVisibility(video ? mediaViewVisibility : false);
        if (albumImg != null)
            albumImage.setImage((new Image(new ByteArrayInputStream(albumImg))));
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        mediaView.setMediaPlayer(mediaPlayer);
    }


}

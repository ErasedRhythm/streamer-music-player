package de.stronnetwork.GUI.MainWindow;

import de.stronnetwork.Settings;
import javafx.scene.control.TextField;

public class MultiStream {
    public TextField firstStreamer;
    public TextField secondStreamer;
    public TextField thirdStreamer;
    private Settings settings = Settings.getSettings();

    public void onMultiUpdate() {
        settings.setMultiFirstStreamer(firstStreamer.textProperty().getValue());
        settings.setMultiSecondStreamer(secondStreamer.textProperty().getValue());
        settings.setMultiThirdStreamer(thirdStreamer.textProperty().getValue());
    }

    public void initialize() {
        firstStreamer.textProperty().setValue(settings.getMultiFirstStreamer());
        secondStreamer.textProperty().setValue(settings.getMultiSecondStreamer());
        thirdStreamer.textProperty().setValue(settings.getMultiThirdStreamer());
    }
}

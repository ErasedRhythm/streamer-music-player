package de.stronnetwork.GUI.MainWindow;

import de.stronnetwork.Player;
import de.stronnetwork.Settings;
import de.stronnetwork.utilities.ProgressBarUpdater;
import de.stronnetwork.utilities.playlist.song.Song;
import javafx.application.Platform;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.concurrent.TimeUnit;

/**
 * The Controller for the Player View of the Player Application.
 * It defines the Actions of the Users possible actions for changing the Playback Media (View).
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.1
 * @since 0.5
 */
public class PlayerViewController {

    public ToggleButton mediaViewToggle;
    public MediaBox MediaBoxController;

    public Slider volumeSlider;
    public TextField volumeField;

    public Text nowPlaying;
    public Slider songProgressSlider;
    public Text songProgressInfo;

    public Text songRating;
    public CheckBox twitchVoting;
    public ToggleButton randomSongsToggleButton;

    private final Player player = Player.getInstance();
    private final Settings settings = Settings.getSettings();

    public void initialize() {
        iniVolume();
        iniTwitchVoting();
        iniPlayer();

        new ProgressBarUpdater(player, this).start();
    }


    private void iniVolume() {
        volumeSlider.valueProperty().setValue(settings.getPlayerVolume());
        volumeField.textProperty().setValue(String.valueOf(settings.getPlayerVolume()));

        volumeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            int volumeValue = (int) (double) newValue;
            Player.getInstance().changeVolume(volumeValue);
            if (!volumeField.isFocused())
                setVolumeText(volumeValue);
        });
    }

    private void iniTwitchVoting() {
        twitchVoting.setSelected(settings.getPlayerTwitchVotingEnabled());
    }

    private void iniPlayer() {
        player.addSongChangeListener(this::onSongUpdate);
        new Thread(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            player.play();
        }).start();
    }

    /**
     * Starts the Media Playback
     */
    public void onButtonPlay() {
        player.play();
    }

    /**
     * Pauses the Media Playback
     */
    public void onButtonPause() {
        player.pause();
    }

    /**
     * Switches the Media Playback to the next Media.
     */
    public void onButtonNextSong() {
        player.nextSong();
    }

    /**
     * Toggles whether the video should show or not, if possible.
     */
    public void updateMediaViewToggle() {
        MediaBoxController.setMediaViewVisibility(mediaViewToggle.selectedProperty().get());
    }

    /**
     * Checks for if the pressed Key is the Up or Down Key. Changes the volume
     * @param event The triggering Event. Used for getting the pressed Key.
     */
    public void onVolumeFieldKeyPressed(KeyEvent event) {
        try {
            int value = Integer.parseInt(volumeField.textProperty().getValue());
            String keyPressed = event.getCode().toString();
            if (keyPressed.equals("DOWN") || keyPressed.equals("UP")) {
                if (keyPressed.equals("DOWN")) value--;
                else if (event.getCode().toString().equals("UP")) value++;
                if (value < 0 || value > 100) return;
                setVolumeText(value);
                player.changeVolume(value);
                setVolumeSlider(value);
                event.consume();
            }
        } catch (NumberFormatException ignored) {}
    }

    /**
     * This method checks whether the value is numeric. If true the numeric value is used as the new Volume setting.
     */
    public void onVolumeFieldKeyTyped() {
        try {
            int value = Integer.parseInt(volumeField.textProperty().getValue());
            if (value < 0) {
                value = 0;
            } else if (value > 100) {
                value = 100;
            }
            player.changeVolume(value);
            setVolumeSlider(value);
        } catch (NumberFormatException e) {
            setVolumeText(settings.getPlayerVolume());
        }
    }

    /**
     * Toggles weather the Twitch chat is able to rate the songs
     */
    public void onTwitchVotingToggle() {
        settings.setPlayerTwitchVotingEnabled(twitchVoting.isSelected());
    }

    private void updateSongRatingText(int voteRating) {
        songRating.textProperty().setValue("Song Rating: " + voteRating);
    }

    private void setVolumeText(int value) {
        volumeField.textProperty().setValue(String.valueOf(value));
    }

    private void setVolumeSlider(int value) {
        volumeSlider.valueProperty().setValue(value);
    }

    /**
     * This Method is for updating the GUI to the current state of Media Playback.
     * @param currentTime The current timestamp of the current Media Playback.
     * @param songLength The total length of the current Media Playback.
     */
    public void setProgressCurrent(Duration currentTime, Duration songLength) {
        Platform.runLater(() -> {
            String currentMinutes = intToTwoCharacterString((int) currentTime.toMinutes());
            String currentSeconds = intToTwoCharacterString((int) currentTime.toSeconds() % 60);
            String totalMinutes = intToTwoCharacterString((int) songLength.toMinutes());
            String totalSeconds = intToTwoCharacterString((int) songLength.toSeconds() % 60);

            songProgressInfo.setText(currentMinutes + ":" + currentSeconds + "/" + totalMinutes + ":" + totalSeconds);
            songProgressSlider.valueProperty().setValue(currentTime.toMillis() / songLength.toMillis() * 100);
        });
    }

    private String intToTwoCharacterString(int i) {
        if (i < 10)
            return "0" + i;
        return i + "";
    }

    /**
     * This Method is called on dragging the Progress Bar and changes the Media Playback to the currently
     * dragged position.
     */
    public void onProgressBarMouseDrag() {
        player.changeSongProgress( songProgressSlider.getValue() / 100);
    }

    private void onSongUpdate(Player.UpdateReason reason, Object newValue) {
        if (reason == Player.UpdateReason.SONG) {
            Song song = (Song) newValue;
            nowPlaying.textProperty().setValue(song.getTitle() + " - " + song.getArtist());
            new Thread(() -> {
                //On Startup these may be null. Wait a little bit and it will get set.
                while (mediaViewToggle.getScene() == null) {
                    try {
                        TimeUnit.MILLISECONDS.sleep(10);
                    } catch (InterruptedException ignore) {}
                }
                while (mediaViewToggle.getScene().getWindow() == null) {
                    try {
                        TimeUnit.MILLISECONDS.sleep(10);
                    } catch (InterruptedException ignore) {}
                }
                Platform.runLater(() -> ((Stage) mediaViewToggle.getScene().getWindow()).setTitle(song.getTitle() + " - " + song.getArtist() + " - Twitch Music Player"));

            }).start();

            MediaBoxController.newSongInfo(song.isVideo(), song.getAlbumImg());
            updateSongRatingText(song.getVotes());
        } else if (reason == Player.UpdateReason.VOLUME) {
            settings.setPlayerVolume((int) newValue);
        } else if (reason == Player.UpdateReason.MEDIAPLAYER) {
            MediaBoxController.setMediaPlayer((MediaPlayer) newValue);
        }
    }

    /**
     * This method toggles whether the Playback should follow the {@link PlaylistViewController shown Playlist} or
     * play these Items at random.
     */
    public void updateRandomSongToggle() {
        Player.getInstance().setRandomSongs(randomSongsToggleButton.selectedProperty().get());
    }

    /**
     * On call this method adds a positive vote with the {@link Settings#getAuthNickname() OAuth Nickname}.
     */
    public void buttonLikeFromGUI() {
        player.getCurrentSong().addVoter(Settings.getSettings().getAuthNickname(), true);
    }
    /**
     * On call this method adds a negative vote with the {@link Settings#getAuthNickname() OAuth Nickname}.
     */
    public void buttonDislikeFromGUI() {
        player.getCurrentSong().addVoter(Settings.getSettings().getAuthNickname(), false);
    }
}

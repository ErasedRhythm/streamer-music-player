package de.stronnetwork.GUI.MainWindow;

import com.github.kiulian.downloader.YoutubeException;
import com.mpatric.mp3agic.InvalidDataException;
import de.stronnetwork.Player;
import de.stronnetwork.utilities.playlist.song.FileSong;
import de.stronnetwork.utilities.playlist.song.Song;
import de.stronnetwork.utilities.SongChangeListener;
import de.stronnetwork.utilities.playlist.song.YouTubeSong;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The Controller for the Playlist View of the Player Application.
 * This view lets the user see and modify the current playlist and switch the currently playing Media with another
 * in the playlist.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.6
 */
public class PlaylistViewController {

    public TableView<Song> songTableView;
    public TextField youTubeInput;
    private boolean itemsMovable = true;

    private static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat("application/x-java-serialized-object");
    private final ArrayList<Song> selections = new ArrayList<>();

    /**
     * This initializes how the fields should be filled when a {@link Song} gets added to the TableView.
     * When any sorting is at hand moving items will get deactivated.
     */
    public void initialize() {
        songTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        songTableView.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("Playing"));
        songTableView.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("Title"));
        songTableView.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("Artist"));
        songTableView.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("Album"));
        songTableView.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("Votes"));


        reloadList();

        for(TableColumn<Song, ?> column: songTableView.getColumns()) {
            column.setComparator((o1, o2) -> {
                itemsMovable = false;
                if (o1 instanceof String && o2 instanceof String) {
                    return ((String) o1).compareTo((String) o2);
                }
                if (o1 instanceof Integer && o2 instanceof Integer) {
                    return ((Integer) o1).compareTo((Integer) o2);
                }
                return 0;
            });
        }

        iniDoubleClick();
        iniItemDrag();
        Player.getInstance().addSongChangeListener((reason, newValue) -> {
            if (reason.equals(Player.UpdateReason.SONG) && newValue instanceof Song) {
                reloadList();
            }
        });
    }

    private void reloadList() {
        songTableView.getItems().clear();
        songTableView.getItems().addAll(Player.getInstance().getPlaylist());
    }

    private void iniDoubleClick() {
        songTableView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2){ // double click

                Song selected = songTableView.getSelectionModel().getSelectedItem();
                if (selected != null) {
                    Player.getInstance().play(songTableView.getSelectionModel().getSelectedIndex());
                }
            }
        });
    }

    private void iniItemDrag() {
        songTableView.setRowFactory(tv -> {
            TableRow<Song> row = new TableRow<>();

            row.setOnDragDetected(event -> onRowDragDetected(row, event));

            row.setOnDragOver(event -> onRowDragOver(row, event));

            row.setOnDragDropped(event -> onRowDragDropped(row, event));

            return row;
        });
    }

    private void onRowDragDropped(TableRow<Song> row, DragEvent event) {
        Dragboard db = event.getDragboard();

        if (db.hasContent(SERIALIZED_MIME_TYPE) && itemsMovable) {

            int dropIndex;
            Song dI = null;

            if (row.isEmpty()) {
                dropIndex = songTableView.getItems().size();
            } else {
                dropIndex = row.getIndex();
                dI = songTableView.getItems().get(dropIndex);
            }
            int delta = 0;
            if (dI != null)
                while (selections.contains(dI)) {
                    delta = 1;
                    --dropIndex;
                    if (dropIndex < 0) {
                        dI = null;
                        dropIndex = 0;
                        break;
                    }
                    dI = songTableView.getItems().get(dropIndex);
                }

            for (Song sI : selections) {
                songTableView.getItems().remove(sI);
            }

            if (dI != null)
                dropIndex = songTableView.getItems().indexOf(dI) + delta;
            else if (dropIndex != 0)
                dropIndex = songTableView.getItems().size();


            songTableView.getSelectionModel().clearSelection();

            for (Song sI : selections) {
                songTableView.getItems().add(dropIndex, sI);
                songTableView.getSelectionModel().select(dropIndex);
                dropIndex++;

            }

            //Player.getInstance().setPlaylist().playlist.getItems(); TODO

            event.setDropCompleted(true);
            selections.clear();
            event.consume();
        }
    }

    private void onRowDragOver(TableRow<Song> row, DragEvent event) {
        Dragboard db = event.getDragboard();
        if (db.hasContent(SERIALIZED_MIME_TYPE) && itemsMovable) {
            if (row.getIndex() != (Integer) db.getContent(SERIALIZED_MIME_TYPE)) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                event.consume();
            }
        }
    }

    private void onRowDragDetected(TableRow<Song> row, MouseEvent event) {
        if (!row.isEmpty() && itemsMovable) {
            Integer index = row.getIndex();

            selections.clear();//important...

            ObservableList<Song> items = songTableView.getSelectionModel().getSelectedItems();

            selections.addAll(items);


            Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
            db.setDragView(row.snapshot(null, null));
            ClipboardContent cc = new ClipboardContent();
            cc.put(SERIALIZED_MIME_TYPE, index);
            db.setContent(cc);
            event.consume();
        }
    }

    public void playlistReset() {
        reloadList();
        itemsMovable = true;
    }

    public void onFileDragDropped(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasFiles()) {
            for (File file:db.getFiles()) {
                addFileToPlaylist(file);
            }
            success = true;
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
    }

    private void addFileToPlaylist(File file) {
        if (file.isDirectory())
            for (File innerFile:file.listFiles()) {
                addFileToPlaylist(innerFile);
            }
        else try {
            Song song = new FileSong(file);
            if (songTableView.getItems().contains(song)) return;
            Player.getInstance().addSongToPlaylist(song);
            songTableView.getItems().add(song);
        } catch (FileNotFoundException | InvalidDataException ignore) {}
    }

    public void onFileDragOver(DragEvent event) {
        if (event.getGestureSource() != songTableView
                && event.getDragboard().hasFiles()) {
            /* allow for both copying and moving, whatever user chooses */
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    public void addYouTubeSongToPlaylist(ActionEvent actionEvent) {
        String input = youTubeInput.textProperty().getValue();
        try {
            YouTubeSong song = new YouTubeSong(input);
            Player.getInstance().addSongToPlaylist(song);
            reloadList();
        } catch (IOException | YouTubeSong.VideoToLongException | YoutubeException ignore) {}
        actionEvent.consume();
    }

    public void onKeyPressedPlaylist(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.DELETE)) {
            ObservableList<Song> selected = songTableView.getSelectionModel().getSelectedItems();
            for (Song song: selected) {
                Player.getInstance().removeSong(song);
            }
            reloadList();
        }
    }
}

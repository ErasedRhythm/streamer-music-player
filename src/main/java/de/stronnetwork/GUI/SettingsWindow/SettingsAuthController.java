package de.stronnetwork.GUI.SettingsWindow;

import de.stronnetwork.Settings;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * The Controller for the the Auth Settings of the Settings Window.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.6
 */
public class SettingsAuthController {

    public TextField authNickname;
    public PasswordField authKey;
    public TextField authChannel;
    private Settings settings = Settings.getSettings();

    /**
     * The initialization is called directly after the Constructor. Initialises every Node.
     */
    public void initialize() {
        authNickname.textProperty().setValue(settings.getAuthNickname());
        authKey.textProperty().setValue(settings.getAuthKey());
        authChannel.textProperty().setValue(settings.getAuthChannel());
    }

    /**
     * Saves every value that this Controller is asking for.
     * @return If any changes done need a restart.
     */
    public boolean save() {
        boolean needRestart = false;
        String nickname = authNickname.textProperty().getValueSafe();
        if (!settings.getAuthNickname().equals(nickname)) {
            settings.setAuthNickname(nickname);
            needRestart = true;
        }

        String authKeyString = authKey.textProperty().getValueSafe();
        if (!settings.getAuthKey().equals(authKeyString)) {
            settings.setAuthKey(authKeyString);
            needRestart = true;
        }

        String channel = authChannel.textProperty().getValueSafe().toLowerCase();
        channel.replace("#", "");
        if (!settings.getAuthChannel().equalsIgnoreCase(channel)) {
            settings.setAuthChannel(authChannel.textProperty().getValueSafe());
            needRestart = true;
        }

        return needRestart;
    }

    /**
     * Opens a Browser (-Tab) with the URL to get the OAuth Key.
     */
    public void openOAuthInBrowser() {
        try {
            Desktop.getDesktop().browse(new URI("https://twitchapps.com/tmi/"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}

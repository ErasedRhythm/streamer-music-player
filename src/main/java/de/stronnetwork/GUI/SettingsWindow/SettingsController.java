package de.stronnetwork.GUI.SettingsWindow;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class SettingsController {

    public SettingsAuthController AuthenticationController;
    public SettingsMusicPlayerController PlayerController;
    public SettingsMoneyController MoneyController;
    public VBox Authentication;
    public ScrollPane Player;
    public TabPane Money;
    public Text needRestart;

    private boolean restart = false;

    public void initialize() {
        needRestart.setVisible(restart);
        needRestart.setManaged(restart);
    }

    public void openAuthenticationSettings() {
        Authentication.setVisible(true);
        Authentication.setManaged(true);
        Player.setVisible(false);
        Player.setManaged(false);
        Money.setVisible(false);
        Money.setManaged(false);
    }

    public void openMusicPlayerSettings() {
        Authentication.setVisible(false);
        Authentication.setManaged(false);
        Player.setVisible(true);
        Player.setManaged(true);
        Money.setVisible(false);
        Money.setManaged(false);
    }

    public void openMoneySettings() {
        Authentication.setVisible(false);
        Authentication.setManaged(false);
        Player.setVisible(false);
        Player.setManaged(false);
        Money.setVisible(true);
        Money.setManaged(true);
    }

    public void saveSettings() {
        if (AuthenticationController.save() || PlayerController.save() || MoneyController.save())
            restart = true;
        System.out.println(restart);
        needRestart.setVisible(restart);
        needRestart.setManaged(restart);
    }
}

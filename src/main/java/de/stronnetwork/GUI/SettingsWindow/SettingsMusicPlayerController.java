package de.stronnetwork.GUI.SettingsWindow;

import de.stronnetwork.GUITemplates;
import de.stronnetwork.Settings;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SettingsMusicPlayerController {

    public VBox upVoteParent;
    public VBox downVoteParent;
    public TextField newUpVote;
    public TextField newDownVote;
    private Settings settings = Settings.getSettings();

    /**
     * The initialization is called directly after the Constructor. Initialises every Node.
     */
    public void initialize() {
        setMusicPlayerUpVoteCommandFields();
        setMusicPlayerDownVoteCommandFields();
    }

    /**
     * The initializer for the up-votes. Adds all up-votes to the window with a removal button.
     */
    private void setMusicPlayerUpVoteCommandFields() {
        try {
            for (String command : settings.getPlayerVotingCommandsGood()) {
                HBox newUpvoteRemovable = GUITemplates.getUpVoteTemplate();
                ((TextField) newUpvoteRemovable.lookup("#upvote")).textProperty().setValue(command);
                ((Button) newUpvoteRemovable.lookup("#removeupvotebutton"))
                        .setOnAction(event -> {
                            newUpvoteRemovable.setVisible(false);
                            newUpvoteRemovable.setManaged(false);
                        });
                upVoteParent.getChildren().add(newUpvoteRemovable);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The initializer for the down-votes. Adds all down-votes to the window with a removal button.
     */
    private void setMusicPlayerDownVoteCommandFields() {
        try {
            for (String command : settings.getPlayerVotingCommandsBad()) {
                HBox newDownvoteRemovable = GUITemplates.getDownVoteTemplate();
                ((TextField) newDownvoteRemovable.lookup("#downvote")).textProperty().setValue(command);
                ((Button) newDownvoteRemovable.lookup("#removedownvotebutton"))
                        .setOnAction(event -> {
                            newDownvoteRemovable.setVisible(false);
                            newDownvoteRemovable.setManaged(false);
                        });
                downVoteParent.getChildren().add(newDownvoteRemovable);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves every value that this Controller is asking for.
     * @return If any changes done need a restart.
     */
    public boolean save() {
        ArrayList<String> upVoteList = new ArrayList<>();
        for (Node p : upVoteParent.getChildren()) {
            if (p.isVisible() && !p.idProperty().isEmpty().get()) {
                HBox parent = (HBox) p;
                TextField textField = (TextField) parent.lookup("#upvote");
                upVoteList.add((textField).textProperty().getValue());
            }
        }
        settings.setPlayerVotingCommandsGood(upVoteList);
        ArrayList<String> downVoteList = new ArrayList<>();
        for (Node p : downVoteParent.getChildren()) {
            if (p.isVisible() && !p.idProperty().isEmpty().get()) {
                HBox parent = (HBox) p;
                downVoteList.add(((TextField) parent.lookup("#downvote")).textProperty().getValue());
            }
        }
        settings.setPlayerVotingCommandsBad(downVoteList);
        return false;
    }

    /**
     * Gets called when the Button for a new up-vote gets pressed. Adds a new up-vote to the settings up-vote list.
     */
    public void addNewUpVote() {
        try {
            HBox newUpVoteRemovable = GUITemplates.getUpVoteTemplate();
            String newCommand = newUpVote.textProperty().getValue();
            newUpVote.textProperty().setValue("");
            if (!newCommand.startsWith("!"))
                newCommand = "!" + newCommand;
            ((TextField) newUpVoteRemovable.lookup("#upvote")).textProperty().setValue(newCommand);
            ((Button) newUpVoteRemovable.lookup("#removeupvotebutton"))
                    .setOnAction(event -> {
                        newUpVoteRemovable.setVisible(false);
                        newUpVoteRemovable.setManaged(false);
                    });
            upVoteParent.getChildren().add(newUpVoteRemovable);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets called when the Button for a new down-vote gets pressed. Adds a new up-vote to the settings down-vote list.
     */
    public void addNewDownVote() {
        try {
            HBox newDownVoteRemovable = GUITemplates.getDownVoteTemplate();
            String newCommand = newDownVote.textProperty().getValue();
            newDownVote.textProperty().setValue("");
            if (!newCommand.startsWith("!"))
                newCommand = "!" + newCommand;
            ((TextField) newDownVoteRemovable.lookup("#downvote")).textProperty().setValue(newCommand);
            ((Button) newDownVoteRemovable.lookup("#removedownvotebutton"))
                    .setOnAction(event -> {
                        newDownVoteRemovable.setVisible(false);
                        newDownVoteRemovable.setManaged(false);
                    });
            downVoteParent.getChildren().add(newDownVoteRemovable);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package de.stronnetwork;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class GUITemplates {
    static String upVoteTemplate = "#upvotetemplate";
    static String downVoteTemplate = "#downvotetemplate";
    static String songFolderTemplate = "#songfoldertemplate";

    private static Scene loadScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(GUITemplates.class.getResource("guiTemplates.fxml"));
        return new Scene(fxmlLoader.load());
    }

    public static HBox getUpVoteTemplate() throws IOException {
        return (HBox) loadScene().lookup(upVoteTemplate);
    }

    public static HBox getDownVoteTemplate() throws IOException {
        return (HBox) loadScene().lookup(downVoteTemplate);
    }

    public static HBox getFolderTemplate() throws IOException {
        return (HBox) loadScene().lookup(songFolderTemplate);
    }
}

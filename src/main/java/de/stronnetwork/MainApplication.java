package de.stronnetwork;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApplication extends Application {

    private static Stage primaryStage;

    public static void main(String[] args) {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        launch(args);
    }

    public static void startup() {
        Settings settings = Settings.getSettings();
        new Player(settings.getPlayerVolume());
        try {
            Parent guiController = FXMLLoader.load(
                    MainApplication.class.getResource("GUI/MainWindow/MainWindow.fxml"));
            Scene scene = new Scene(guiController);
            primaryStage.setScene(scene);
            primaryStage.setMinWidth(primaryStage.minWidthProperty().getValue());
            primaryStage.setMinHeight(scene.getHeight());
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setOnCloseRequest(e -> {
            if (Player.getInstance() != null)
                Player.getInstance().pause();
            Platform.exit();
            System.exit(0);
        });
        MainApplication.primaryStage = primaryStage;
        Settings settings = Settings.getSettings();
        if (settings.isNewFile() || settingsCrucialFieldsNotSet()) {
            Parent guiController = FXMLLoader.load(getClass().getResource("GUI/FirstStartUpWindow/guiFirstSetup.fxml"));
            Scene scene = new Scene(guiController);
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        else startup();
    }

    private boolean settingsCrucialFieldsNotSet() {
        Settings settings = Settings.getSettings();
        return (settings.getAuthChannel().isEmpty() ||
                settings.getAuthNickname().isEmpty() ||
                settings.getAuthKey().isEmpty());
    }


}

package de.stronnetwork;

import de.stronnetwork.utilities.*;
import de.stronnetwork.utilities.playlist.song.Song;
import de.stronnetwork.utilities.playlist.PlaylistHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.util.*;

/**
 * A Player for playing {@link Song Songs}. The playable songs are defined in {@link Settings} and through
 * {@link #addSongRequest song requests}.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0.1
 * @since 0.1
 */
public class Player {

    public void setRandomSongs(boolean b) {
        songLibrary.setRandomSong(b);
    }

    public void addSongToPlaylist(Song song) {
        songLibrary.addSongToCurrentPlaylist(song);
    }

    /**
     * Starts playback of the given index. This method allows a specific song to be played instead of a song that is selected by {@link PlaylistHandler}.
     * @param selectedIndex The song to be played.
     */
    public void play(int selectedIndex) {
        Expected.greaterThen(selectedIndex, 0);
        if (mediaPlayer != null) {
            mediaPlayer.dispose();
        }
        Song song = songLibrary.getAndSetAsCurrent(selectedIndex);
        song.resetVoters();
        mediaPlayer = new MediaPlayer(new Media(song.getURI()));
        mediaPlayer.setOnEndOfMedia(this::nextSong);
        play();
    }

    public void removeSong(Song song) {
        songLibrary.removeSong(song);
    }

    /**
     * All possible reasons the player may send an Update to the {@link SongChangeListener SongChangeListeners}.
     */
    public enum UpdateReason {VOLUME, PLAYING, MEDIAPLAYER, SONG}

    private final PlaylistHandler songLibrary;

    private MediaPlayer mediaPlayer;
    private boolean playing = false;
    private int currentVolume = 10;

    private ArrayList<SongChangeListener> onSongUpdateList = new ArrayList<>();

    static private Player player;

    /**
     * This Constructor is only to be done <em>once</em>. Use {@link #getInstance()} after first initialisation.
     * @param initVolume The volume the Player should initialise with.
     */
    public Player(int initVolume) {
        Expected.isNull(player);
        player = this;
        changeVolume(initVolume);
        this.songLibrary = new PlaylistHandler();
    }

    /**
     * Should be called every time <em>after</em> the Constructor got called once to get the Instance.
     * @return Returns the Instance of the Player. If not initialized null.
     */
    static public Player getInstance() {
        return player;
    }

    /**
     * Returns the current song. See {@link PlaylistHandler#getCurrentSong()}.
     * @return The current Song. May be Null.
     */
    public Song getCurrentSong() {
        return songLibrary.getCurrentSong();
    }

    /**
     * Resumes the playback of the Media. If no song was playing a song will be selected.
     */
    public void play() {
        if (songLibrary.isEmpty()) return;
        if (mediaPlayer == null) nextSong();
        changeVolume(currentVolume);
        mediaPlayer.play();
        playing = true;
        updateSongInfoToListenersOnPlay();
    }

    /**
     * @return True if the media is playing.
     */
    public boolean isPlaying() {
        return playing;
    }

    /**
     * Pauses the playback of the Media.
     */
    public void pause() {
        if (mediaPlayer != null)
            mediaPlayer.pause();
        playing = false;
        notifySongUpdateListeners(UpdateReason.PLAYING, false);
    }

    /**
     * Starts the playback of the next song and stops the playback of the current song if one is playing.
     * For more info what the next song might be see {@link PlaylistHandler#getNextSong()}.
     */
    public void nextSong() {
        if (songLibrary.isEmpty()) return;
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.dispose();
        }
        mediaPlayer = new MediaPlayer(new Media(songLibrary.getNextSong().getURI()));
        songLibrary.getCurrentSong().resetVoters();
        mediaPlayer.setOnEndOfMedia(this::nextSong);
        play();
    }

    private void updateSongInfoToListenersOnPlay() {
        notifySongUpdateListeners(UpdateReason.PLAYING, true);
        notifySongUpdateListeners(UpdateReason.SONG, songLibrary.getCurrentSong());
        notifySongUpdateListeners(UpdateReason.MEDIAPLAYER, mediaPlayer);
    }

    /**
     * Changes the Volume of the playing Song. The value has to be between 0 and 100.
     * If no Song is currently playing, the new volume will be applied for when the player gets an {@link #play()}
     * or {@link #nextSong()}.
     *
     * @param volume The new Volume of the current or next Song.
     */
    public void changeVolume(int volume) {
        Expected.between(volume, 0, 100);
        currentVolume = volume;
        if (mediaPlayer != null) {
            mediaPlayer.setVolume(volume / 99.0);
        }
        notifySongUpdateListeners(UpdateReason.VOLUME, currentVolume);
    }

    /**
     * Changes the playback time. The value has to be between 0.0 and 1.0 inclusive.
     * @param newValue The playback time between 0.0 and 1.0.
     */
    public void changeSongProgress(double newValue) {
        Expected.between(newValue, 0d, 1d);
        Duration duration = new Duration(newValue * mediaPlayer.getMedia().getDuration().toMillis());
        // This is needed for the SongProgressUpdater.
        // Without it the change would trigger every time the Progress Bar moves.
        if (Math.abs(duration.toSeconds() - mediaPlayer.getCurrentTime().toSeconds()) > 1)
            mediaPlayer.seek(duration);
    }

    private void notifySongUpdateListeners(UpdateReason reason, Object changed) {
        for (SongChangeListener listener : onSongUpdateList) {
            listener.onSongNameUpdate(reason, changed);
        }
    }

    /**
     * Adds an {@link SongChangeListener} to the player. The Listener will get updates in form of {@link UpdateReason}.
     * @param listener The listener to add.
     */
    public void addSongChangeListener(SongChangeListener listener) {
        Expected.notNull(listener);
        onSongUpdateList.add(listener);
    }

    /**
     * @return Returns the current timestamp of the current Media Playback.
     */
    public Duration getCurrentTime() {
        if (mediaPlayer == null) return null;
        return mediaPlayer.getCurrentTime();
    }

    /**
     * @return Returns the total song length of the current Media Playback.
     */
    public Duration getSongLength() {
        return mediaPlayer.getTotalDuration();
    }

    /**
     * Adds a Song to the back of the queue. More info to the queue is {@link PlaylistHandler#getNextSong() here}.
     * @param song The song that should be added to the queue.
     */
    public void addSongRequest(Song song) {
        songLibrary.addToQueue(song);
    }

    public ArrayList<Song> getPlaylist() {
        return songLibrary.getPlaylist();
    }
}

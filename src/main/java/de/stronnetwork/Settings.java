package de.stronnetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.ArrayList;

public class Settings {
    static private Settings settings;
    private String fileName = "settings.json";
    private File settingsFile;
    private boolean fileReadingError = false;
    private boolean fileWritingError = false;
    private boolean newFile = false;
    private boolean debug;

    private String authKey;
    private String authNickname;
    private String authChannel;

    private int playerVolume = 10;
    private boolean playerTwitchVotingEnabled = false;
    private ArrayList<String> playerVotingCommandsGood = new ArrayList<>();
    private ArrayList<String> playerVotingCommandsBad = new ArrayList<>();

    private String multiFirstStreamer = "";
    private String multiSecondStreamer = "";
    private String multiThirdStreamer = "";

    private Boolean moneyActivated = false;
    private String moneyName = "Coins";
    private ArrayList<String> moneyCheckCommands = new ArrayList<>();
    private int moneyTimeAwardAmount = 10;
    private int moneyTimeAwardMinutes = 1;
    private int moneyAwardPerMessage = 10;
    private ArrayList<String> moneyBannedClients = new ArrayList<>();

    private Boolean slotmachineActivated = false;
    private ArrayList<String> slotmachineGoodSlots = new ArrayList<>();
    private ArrayList<String> slotmachineBadSlots = new ArrayList<>();
    private int slotmachineMinStake = 1;
    private int slotmachineMaxStake = 1000;
    private int slotmachineThreeSameGood = 10;
    private int slotmachineThreeGood = 5;
    private int slotmachineTwoGood = 1;
    private int slotmachineThreeSameBad = 3;

    private Settings() throws IOException {
        settingsFile = new File(fileName);
        if (!settingsFile.exists()) {
            createDefaultFile(settingsFile);
            newFile = true;
        } else if (!settingsFile.canRead()) {
            System.err.println(
                    "Config file is not readable! Starting with default settings. Will not update saved Settings till the error is solved.");
            fileReadingError = true;
            return;
        } else if (!settingsFile.canWrite()) {
            System.err.println("Config file not writable! Will not update saved Settings till the error is solved.");
            fileWritingError = true;
        }
        readSettings(settingsFile);
    }

    public static Settings getSettings() {
        if (settings == null) {
            try {
                settings = new Settings();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return settings;
    }

    private void readSettings(File settingsFile) throws JSONException, FileNotFoundException {
        JSONObject json = new JSONObject(new JSONTokener(new FileReader(settingsFile)));
        debug = json.getBoolean("debug");
        setAuthentificationSettings(json.getJSONObject("Authentification"));

        JSONObject functionsSettings = json.getJSONObject("Functions");
        setPlayerSettings(functionsSettings.getJSONObject("SongPlayer"));
        setMoneySettings(functionsSettings.getJSONObject("Money"));
        setMultiSetting(functionsSettings.getJSONObject("Multi"));
    }

    private void setMultiSetting(JSONObject multi) {
        multiFirstStreamer = multi.getString("firstStreamer");
        multiSecondStreamer = multi.getString("secondStreamer");
        multiThirdStreamer = multi.getString("thirdStreamer");
    }

    private void setMoneySettings(JSONObject jsonObject) {
        moneyActivated = jsonObject.getBoolean("Activated");
        moneyName = jsonObject.getString("MoneyName");
        for (Object object : jsonObject.getJSONArray("MoneyCheckCommands")) {
            moneyCheckCommands.add(object.toString());
        }
        JSONObject moneyAwards = jsonObject.getJSONObject("MoneyAwards");
        moneyTimeAwardAmount = moneyAwards.getJSONObject("TimeAward").getInt("Award");
        moneyTimeAwardMinutes = moneyAwards.getJSONObject("TimeAward").getInt("TimeInMinutes");
        moneyAwardPerMessage = moneyAwards.getInt("PerChatMessage");
        for (Object object : jsonObject.getJSONArray("bannedUsersForMoneyAwards")) {
            moneyBannedClients.add(object.toString());
        }

        setSlotmachineSettings(jsonObject.getJSONObject("SlotMachine"));
    }

    private void setSlotmachineSettings(JSONObject jsonObject) {
        slotmachineActivated = jsonObject.getBoolean("Activated");
        for (Object object : jsonObject.getJSONArray("GoodSlots")) {
            slotmachineGoodSlots.add(object.toString());
        }
        for (Object object : jsonObject.getJSONArray("BadSlots")) {
            slotmachineBadSlots.add(object.toString());
        }
        slotmachineMinStake = jsonObject.getInt("MinSlotsStake");
        slotmachineMaxStake = jsonObject.getInt("MaxSlotsStake");
        JSONObject rewards = jsonObject.getJSONObject("Rewards");
        slotmachineThreeSameGood = rewards.getInt("ThreeSameGood");
        slotmachineThreeGood = rewards.getInt("ThreeGood");
        slotmachineTwoGood = rewards.getInt("TwoGood");
        slotmachineThreeSameBad = rewards.getInt("ThreeSameBad");

    }

    private void setPlayerSettings(JSONObject jsonObject) {
        playerVolume = jsonObject.getInt("Volume");
        playerTwitchVotingEnabled = jsonObject.getBoolean("TwitchVotingEnabled");
        for (Object object : jsonObject.getJSONArray("VotingGood")) {
            playerVotingCommandsGood.add(object.toString());
        }
        for (Object object : jsonObject.getJSONArray("VotingBad")) {
            playerVotingCommandsBad.add(object.toString());
        }
    }

    private void setAuthentificationSettings(JSONObject jsonObject) {
        authKey = jsonObject.getString("OAuthKey");
        authNickname = jsonObject.getString("Nickname");
        authChannel = jsonObject.getString("Channel");
    }

    private void createDefaultFile(File settingsFile) throws IOException {
        JSONObject settings = new JSONObject();
        settings.put("debug", false);
        settings.put("Authentification", createAuthJsonObject());
        settings.put("Functions", createFuncJsonObject());
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(settings.toString(2));
        }
    }

    private Object createFuncJsonObject() {
        JSONObject functions = new JSONObject();
        functions.put("SongPlayer", createSongPlayerObject());
        functions.put("Money", createMoneyObject());
        functions.put("Multi", createMultiObject());

        return functions;
    }

    private JSONObject createMultiObject() {
        JSONObject multi = new JSONObject();
        multi.put("firstStreamer", "");
        multi.put("secondStreamer", "");
        multi.put("thirdStreamer", "");
        return multi;
    }

    private Object createMoneyObject() {
        JSONObject money = new JSONObject();
        money.put("Activated", false);
        money.put("MoneyName", "Coins");
        money.put("MoneyCheckCommands", new JSONArray());
        JSONObject moneyAwards = new JSONObject();
        moneyAwards.put("TimeAward", new JSONObject().put("Award", 10).put("TimeInMinutes", 1));
        moneyAwards.put("PerChatMessage", 10);
        money.put("MoneyAwards", moneyAwards);
        money.put("SlotMachine", createSlotMachineObject());
        money.put("bannedUsersForMoneyAwards", new JSONArray());
        return money;
    }

    private Object createSlotMachineObject() {
        JSONObject slots = new JSONObject();
        slots.put("Activated", false);
        slots.put("GoodSlots", new String[]{"PogChamp", "BlessRNG"});
        slots.put("BadSlots", new String[]{"WutFace", "DansGame"});
        slots.put("MinSlotsStake", 1);
        slots.put("MaxSlotsStake", 1000);
        slots.put("Rewards",
                new JSONObject().put("ThreeSameGood", 10).put("ThreeGood", 5).put("TwoGood", 1).put("ThreeSameBad", 3));

        return slots;
    }

    private JSONObject createSongPlayerObject() {
        JSONObject songPlayer = new JSONObject();
        songPlayer.put("SongLibrary", new JSONArray());
        songPlayer.put("Volume", 10);
        songPlayer.put("TwitchVotingEnabled", false);
        songPlayer.put("VotingGood", new String[]{"!like"});
        songPlayer.put("VotingBad", new String[]{"!dislike"});
        return songPlayer;
    }

    private JSONObject createAuthJsonObject() {
        JSONObject auth = new JSONObject();
        auth.put("OAuthKey", "");
        auth.put("Nickname", "");
        auth.put("Channel", "");
        return auth;
    }

    /**
     * @return the fileReadingError
     */
    public boolean isFileReadingError() {
        return fileReadingError;
    }

    /**
     * @return the fileWritingError
     */
    public boolean isFileWritingError() {
        return fileWritingError;
    }

    /**
     * @return the authKey
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * @param authKey the authKey to set
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Authentification").put("OAuthKey", authKey);
        writeToFile(json);
    }

    /**
     * @return the authNickname
     */
    public String getAuthNickname() {
        return authNickname;
    }

    /**
     * @param authNickname the authNickname to set
     */
    public void setAuthNickname(String authNickname) {
        this.authNickname = authNickname;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Authentification").put("Nickname", authNickname);
        writeToFile(json);
    }

    /**
     * @return the authChannel
     */
    public String getAuthChannel() {
        return authChannel;
    }

    /**
     * @param authChannel the authChannel to set
     */
    public void setAuthChannel(String authChannel) {
        this.authChannel = authChannel;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Authentification").put("Channel", authChannel);
        writeToFile(json);
    }

    /**
     * @return the playerVolume
     */
    public Integer getPlayerVolume() {
        return playerVolume;
    }

    /**
     * @param playerVolume the playerVolume to set
     */
    public void setPlayerVolume(int playerVolume) {
        this.playerVolume = playerVolume;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("SongPlayer").put("Volume", playerVolume);
        writeToFile(json);
    }

    /**
     * @return the playerTwitchVotingEnabled
     */
    public Boolean getPlayerTwitchVotingEnabled() {
        return playerTwitchVotingEnabled;
    }

    /**
     * @param playerTwitchVotingEnabled the playerTwitchVotingEnabled to set
     */
    public void setPlayerTwitchVotingEnabled(boolean playerTwitchVotingEnabled) {
        this.playerTwitchVotingEnabled = playerTwitchVotingEnabled;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("SongPlayer").put("TwitchVotingEnabled",
                playerTwitchVotingEnabled);
        writeToFile(json);
    }

    /**
     * @return the playerVotingCommandsGood
     */
    public ArrayList<String> getPlayerVotingCommandsGood() {
        return playerVotingCommandsGood;
    }

    /**
     * @param playerVotingCommandsGood the playerVotingCommandsGood to set
     */
    public void setPlayerVotingCommandsGood(ArrayList<String> playerVotingCommandsGood) {
        this.playerVotingCommandsGood = playerVotingCommandsGood;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("SongPlayer").put("VotingGood", playerVotingCommandsGood);
        writeToFile(json);
    }

    /**
     * @return the playerVotingCommandsBad
     */
    public ArrayList<String> getPlayerVotingCommandsBad() {
        return playerVotingCommandsBad;
    }

    /**
     * @param playerVotingCommandsBad the playerVotingCommandsBad to set
     */
    public void setPlayerVotingCommandsBad(ArrayList<String> playerVotingCommandsBad) {
        this.playerVotingCommandsBad = playerVotingCommandsBad;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("SongPlayer").put("VotingBad", playerVotingCommandsBad);
        writeToFile(json);
    }

    /**
     * @return the moneyActivated
     */
    public Boolean isMoneyActivated() {
        return moneyActivated;
    }

    /**
     * @return the moneyName
     */
    public String getMoneyName() {
        return moneyName;
    }

    /**
     * @param moneyName the moneyName to set
     */
    public void setMoneyName(String moneyName) {
        this.moneyName = moneyName;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").put("MoneyName", moneyName);
        writeToFile(json);
    }

    /**
     * @return the moneyTimeAwardAmount
     */
    public int getMoneyTimeAwardAmount() {
        return moneyTimeAwardAmount;
    }

    /**
     * @param moneyTimeAwardAmount the moneyTimeAwardAmount to set
     */
    public void setMoneyTimeAwardAmount(int moneyTimeAwardAmount) {
        this.moneyTimeAwardAmount = moneyTimeAwardAmount;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").put("MoneyName", moneyTimeAwardAmount);
        writeToFile(json);
    }

    /**
     * @return the moneyTimeAwardMinutes
     */
    public int getMoneyTimeAwardMinutes() {
        return moneyTimeAwardMinutes;
    }

    /**
     * @param moneyTimeAwardMinutes the moneyTimeAwardMinutes to set
     */
    public void setMoneyTimeAwardMinutes(int moneyTimeAwardMinutes) {
        this.moneyTimeAwardMinutes = moneyTimeAwardMinutes;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("MoneyAwards").getJSONObject("TimeAward")
                .put("TimeInMinutes", moneyTimeAwardMinutes);
        writeToFile(json);
    }

    /**
     * @return the moneyAwardPerMessage
     */
    public int getMoneyAwardPerMessage() {
        return moneyAwardPerMessage;
    }

    /**
     * @param moneyAwardPerMessage the moneyAwardPerMessage to set
     */
    public void setMoneyAwardPerMessage(int moneyAwardPerMessage) {
        this.moneyAwardPerMessage = moneyAwardPerMessage;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("MoneyAwards").put("PerChatMessage",
                moneyAwardPerMessage);
        writeToFile(json);
    }

    /**
     * @return the moneyBannedClients
     */
    public ArrayList<String> getMoneyBannedClients() {
        return moneyBannedClients;
    }

    /**
     * @param moneyBannedClients the moneyBannedClients to set
     */
    public void setMoneyBannedClients(ArrayList<String> moneyBannedClients) {
        this.moneyBannedClients = moneyBannedClients;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").put("bannedUsersForMoneyAwards", moneyBannedClients);
        writeToFile(json);
    }

    /**
     * @return the slotmachineActivated
     */
    public Boolean getSlotmachineActivated() {
        return slotmachineActivated;
    }

    /**
     * @param slotmachineActivated the slotmachineActivated to set
     */
    public void setSlotmachineActivated(Boolean slotmachineActivated) {
        this.slotmachineActivated = slotmachineActivated;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").put("Activated",
                moneyBannedClients);
        writeToFile(json);
    }

    /**
     * @return the slotmachineGoodSlots
     */
    public ArrayList<String> getSlotmachineGoodSlots() {
        return slotmachineGoodSlots;
    }

    /**
     * @param slotmachineGoodSlots the slotmachineGoodSlots to set
     */
    public void setSlotmachineGoodSlots(ArrayList<String> slotmachineGoodSlots) {
        this.slotmachineGoodSlots = slotmachineGoodSlots;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").put("GoodSlots",
                slotmachineGoodSlots);
        writeToFile(json);
    }

    /**
     * @return the slotmachineBadSlots
     */
    public ArrayList<String> getSlotmachineBadSlots() {
        return slotmachineBadSlots;
    }

    /**
     * @param slotmachineBadSlots the slotmachineBadSlots to set
     */
    public void setSlotmachineBadSlots(ArrayList<String> slotmachineBadSlots) {
        this.slotmachineBadSlots = slotmachineBadSlots;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").put("BadSlots",
                slotmachineBadSlots);
        writeToFile(json);
    }

    /**
     * @return the slotmachineThreeSameGood
     */
    public int getSlotmachineThreeSameGood() {
        return slotmachineThreeSameGood;
    }

    /**
     * @param slotmachineThreeSameGood the slotmachineThreeSameGood to set
     */
    public void setSlotmachineThreeSameGood(int slotmachineThreeSameGood) {
        this.slotmachineThreeSameGood = slotmachineThreeSameGood;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").getJSONObject("Rewards")
                .put("ThreeSameGood", slotmachineThreeSameGood);
        writeToFile(json);
    }

    /**
     * @return the slotmachineThreeGood
     */
    public int getSlotmachineThreeGood() {
        return slotmachineThreeGood;
    }

    /**
     * @param slotmachineThreeGood the slotmachineThreeGood to set
     */
    public void setSlotmachineThreeGood(int slotmachineThreeGood) {
        this.slotmachineThreeGood = slotmachineThreeGood;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").getJSONObject("Rewards")
                .put("ThreeGood", slotmachineThreeGood);
        writeToFile(json);
    }

    /**
     * @return the slotmachineTwoGood
     */
    public int getSlotmachineTwoGood() {
        return slotmachineTwoGood;
    }

    /**
     * @param slotmachineTwoGood the slotmachineTwoGood to set
     */
    public void setSlotmachineTwoGood(int slotmachineTwoGood) {
        this.slotmachineTwoGood = slotmachineTwoGood;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").getJSONObject("Rewards")
                .put("TwoGood", slotmachineTwoGood);
        writeToFile(json);
    }

    /**
     * @return the slotmachineThreeSameBad
     */
    public int getSlotmachineThreeSameBad() {
        return slotmachineThreeSameBad;
    }

    /**
     * @param slotmachineThreeSameBad the slotmachineThreeSameBad to set
     */
    public void setSlotmachineThreeSameBad(int slotmachineThreeSameBad) {
        this.slotmachineThreeSameBad = slotmachineThreeSameBad;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").getJSONObject("Rewards")
                .put("ThreeSameBad", slotmachineThreeSameBad);
        writeToFile(json);
    }

    /**
     * @return the moneyActivated
     */
    public Boolean getMoneyActivated() {
        return moneyActivated;
    }

    /**
     * @param moneyActivated the moneyActivated to set
     */
    public void setMoneyActivated(Boolean moneyActivated) {
        this.moneyActivated = moneyActivated;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").put("Activated", moneyActivated);
        writeToFile(json);
    }

    private JSONObject getJSONFromFile() {
        try {
            return new JSONObject(new JSONTokener(new FileReader(settingsFile)));
        } catch (JSONException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return the moneyCheckCommands
     */
    public ArrayList<String> getMoneyCheckCommands() {
        return moneyCheckCommands;
    }

    /**
     * @param moneyCheckCommands the moneyCheckCommands to set
     */
    public void setMoneyCheckCommands(ArrayList<String> moneyCheckCommands) {
        this.moneyCheckCommands = moneyCheckCommands;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").put("MoneyCheckCommands", moneyCheckCommands);
        writeToFile(json);
    }

    /**
     * @return the slotmachineMinStake
     */
    public int getSlotmachineMinStake() {
        return slotmachineMinStake;
    }

    /**
     * @param slotmachineMinStake the slotmachineMinStake to set
     */
    public void setSlotmachineMinStake(int slotmachineMinStake) {
        this.slotmachineMinStake = slotmachineMinStake;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").put("MinSlotsStake", slotmachineMinStake);
        writeToFile(json);
    }

    /**
     * @return the slotmachineMaxStake
     */
    public int getSlotmachineMaxStake() {
        return slotmachineMaxStake;
    }

    /**
     * @param slotmachineMaxStake the slotmachineMaxStake to set
     */
    public void setSlotmachineMaxStake(int slotmachineMaxStake) {
        this.slotmachineMaxStake = slotmachineMaxStake;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Money").getJSONObject("SlotMachine").put("MaxSlotsStake", slotmachineMaxStake);
        writeToFile(json);
    }

    private void writeToFile(JSONObject json) {
        try {
            FileWriter file = new FileWriter(fileName);
            file.write(json.toString(2));
            file.close();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public boolean isNewFile() {
        return newFile;
    }

    public String getMultiFirstStreamer() {
        return multiFirstStreamer;
    }

    public void setMultiFirstStreamer(String multiFirstStreamer) {
        this.multiFirstStreamer = multiFirstStreamer;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Multi").put("firstStreamer", multiFirstStreamer);
        writeToFile(json);
    }

    public String getMultiSecondStreamer() {
        return multiSecondStreamer;
    }

    public void setMultiSecondStreamer(String multiSecondStreamer) {
        this.multiSecondStreamer = multiSecondStreamer;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Multi").put("secondStreamer", multiSecondStreamer);
        writeToFile(json);
    }

    public String getMultiThirdStreamer() {
        return multiThirdStreamer;
    }

    public void setMultiThirdStreamer(String multiThirdStreamer) {
        this.multiThirdStreamer = multiThirdStreamer;
        if (isFileWritingError())
            return;
        JSONObject json = getJSONFromFile();
        json.getJSONObject("Functions").getJSONObject("Multi").put("thirdStreamer", multiThirdStreamer);
        writeToFile(json);
    }

    public boolean getPlayerSongRequestActive() {
        //TODO Add Settings for PlayerSongRequests
        return true;
    }

    public long getSlotMachineCoolDownMinutes() {
        //TODO Add Settings for SlotMachineCoolDownMinutes
        return 10;
    }
}

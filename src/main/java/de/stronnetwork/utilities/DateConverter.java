package de.stronnetwork.utilities;

import java.time.LocalDateTime;

public class DateConverter {

    static public LocalDateTime toDateFormater(String string) {
        if (string == null) throw new NullPointerException();
        if (string.isEmpty()) throw new IllegalArgumentException();
        return LocalDateTime.parse(string);
    }

    static public String toString(LocalDateTime date) {
        if (date == null) throw new NullPointerException();
        return date.toString();
    }

}

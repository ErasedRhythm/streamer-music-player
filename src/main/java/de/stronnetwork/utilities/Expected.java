package de.stronnetwork.utilities;

import de.stronnetwork.Player;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * A Class for testing parameters and variables. A constructed object is not needed for any Methods.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.6
 */
public class Expected {
    /**
     * Checks whether the Object given is not Null.
     * @param obj The Object to Check.
     * @throws NullPointerException If the Object is Null.
     */
    public static void notNull(Object obj) throws NullPointerException {
        if (obj == null) throw new NullPointerException();
    }

    /**
     * Checks whether the Object given is not Null. The thrown Error will have a specified Message.
     * @param obj The Object to Check.
     * @param errorMessage The Message to if the Object is Null.
     * @throws NullPointerException If the Object is Null.
     */
    public static void notNull(Object obj, String errorMessage) throws NullPointerException {
        if (obj == null) throw new NullPointerException(errorMessage);
    }

    /**
     * Checks whether the Object given is Null. The thrown Error will have a specified Message.
     * @param obj The Object to Check.
     * @throws IllegalArgumentException If the Object is not Null.
     */
    public static void isNull(Object obj) throws IllegalArgumentException {
        if (obj != null) throw new IllegalArgumentException();
    }
    /**
     * Checks whether the List given is not Empty.
     * @param list The List to Check.
     * @throws IllegalArgumentException If the given List is Empty.
     * @throws NullPointerException If the given List is Null.
     */
    public static void listNotEmpty(List<Object> list) throws IllegalArgumentException {
        notNull(list);
        if (list.isEmpty()) throw new IllegalArgumentException();
    }

    /**
     * Checks whether the List given is not Empty. The thrown Error will have a specified Message.
     * The error message is only set for the {@link IllegalArgumentException}.
     * @param list The List to Check.
     * @param errorMessage The Message to if the List is Empty.
     * @throws IllegalArgumentException If the given List is Empty.
     * @throws NullPointerException If the given List is Null.
     */
    public static void listNotEmpty(List<String> list, String errorMessage) throws IllegalArgumentException {
        notNull(list);
        if (list.isEmpty()) throw new IllegalArgumentException(errorMessage);
    }

    /**
     * Checks whether the file or directory denoted by this abstract pathname exists.
     * @param file The File to Check.
     * @throws FileNotFoundException If the given File does not Exist.
     * @throws NullPointerException If the given List is Null.
     */
    public static void fileExists(File file) throws FileNotFoundException {
        notNull(file);
        if (!file.exists()) throw new FileNotFoundException();
    }

    /**
     * Checks whether the value is between inclusive the min and max value.
     * @param value The value to check.
     * @param min The min value, inclusive.
     * @param max The max value, inclusive.
     * @throws IllegalArgumentException If the value is not between the min and max values, inclusive.
     * @throws NullPointerException If one of the given parameters is Null.
     */
    public static void between(int value, int min, int max) throws IllegalArgumentException {
        greaterThen(value, min);
        lessThen(value, max);
    }

    /**
     * Checks whether the value is greater then the minimum value, inclusive.
     * @param value The value to check.
     * @param min The min value, inclusive.
     * @throws IllegalArgumentException If the value is less than the min value.
     * @throws NullPointerException If one of the given parameters is Null.
     */
    public static void greaterThen (int value, int min) throws IllegalArgumentException {
        notNull(value);
        notNull(min);
        if (value < min) throw new IllegalArgumentException();
    }

    /**
     * Checks whether the value is less then the minimum value, inclusive.
     * @param value The value to check.
     * @param max The max value, inclusive
     * @throws IllegalArgumentException If the value is greater than the max value.
     * @throws NullPointerException If one of the given parameters is Null.
     */
    public static void lessThen(int value, int max) throws IllegalArgumentException {
        notNull(value);
        notNull(max);
        if (value > max) throw new IllegalArgumentException();
    }

    /**
     * Checks whether the value is between inclusive the min and max value.
     * @param value The value to check.
     * @param min The min value, inclusive.
     * @param max The max value, inclusive.
     * @throws IllegalArgumentException If the value is not between the min and max values, inclusive.
     * @throws NullPointerException If one of the given parameters is Null.
     */
    public static void between(double value, double min, double max) throws IllegalArgumentException {
        greaterThen(value, min);
        lessThen(value, max);
    }

    /**
     * Checks whether the value is greater then the minimum value, inclusive.
     * @param value The value to check.
     * @param min The min value, inclusive.
     * @throws IllegalArgumentException If the value is less than the min value.
     * @throws NullPointerException If one of the given parameters is Null.
     */
    private static void greaterThen(double value, double min) throws IllegalArgumentException {
        notNull(value);
        notNull(min);
        if (value < min) throw new IllegalArgumentException();
    }

    /**
     * Checks whether the value is less then the minimum value, inclusive.
     * @param value The value to check.
     * @param max The max value, inclusive
     * @throws IllegalArgumentException If the value is greater than the max value.
     * @throws NullPointerException If one of the given parameters is Null.
     */
    private static void lessThen(double value, double max) throws IllegalArgumentException {
        notNull(value);
        notNull(max);
        if (value > max) throw new IllegalArgumentException();
    }
}

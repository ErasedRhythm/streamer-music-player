package de.stronnetwork.utilities;

import de.stronnetwork.GUI.MainWindow.PlayerViewController;
import de.stronnetwork.Player;

import java.util.concurrent.TimeUnit;

/**
 * This Class updates the ProgressBar of the specified Controller.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0.1
 * @since 0.1
 */
public class ProgressBarUpdater extends Thread {

    private final Player player;
    private final PlayerViewController controller;
    private Boolean running;

    /**
     * Constructor of this class. Needs the Player to access the current timestamp and the Controller to change the ProgressBar.
     * @param player The Media Player to access the timestamp.
     * @param controller The Controller with the ProgressBar.
     */
    public ProgressBarUpdater(Player player, PlayerViewController controller) {
        this.player = player;
        this.controller = controller;
        running = false;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                TimeUnit.MILLISECONDS.sleep(100);
                if (player.isPlaying())
                    controller.setProgressCurrent(player.getCurrentTime(), player.getSongLength());
            } catch (InterruptedException e) {
                running = false;
            } catch (NullPointerException ignored) {
            }
        }
    }
}

package de.stronnetwork.utilities;

import de.stronnetwork.Settings;
import de.stronnetwork.utilities.client.Client;
import de.stronnetwork.utilities.twitch.TwitchUtility;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * A Class to play on a Slot Machine for multiple users at once.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.4.1
 */
public class SlotMachine {

    private static class Slot {
        public String name;
        public boolean winning;
        protected Slot(String name, boolean winning) {
            Expected.notNull(name);
            this.name = name;
            this.winning = winning;
        }
    }
    final private ArrayList<Slot> slotImgs = new ArrayList<>();
    private TwitchUtility utility;
    private Settings settings;
    private HashMap<String, LocalDateTime> lastSlots = new HashMap<>();

    /**
     * Constructs and returns a new SlotMachine Object. {@link Settings} is used for further definitions.
     * The {@link TwitchUtility} is needed for Writing back to the Chat User which is using the SlotMachine.
     * @param utility 
     */
    public SlotMachine(TwitchUtility utility) {
        Expected.notNull(utility);
        this.utility = utility;

        settings = Settings.getSettings();

        Expected.listNotEmpty(Collections.singletonList(settings.getSlotmachineGoodSlots()));
        for (String slot : settings.getSlotmachineGoodSlots()) {
            slotImgs.add(new Slot(slot, true));
        }

        Expected.listNotEmpty(Collections.singletonList(settings.getSlotmachineBadSlots()));
        for (String slot : settings.getSlotmachineBadSlots()) {
            slotImgs.add(new Slot(slot, false));
        }
    }

    /**
     * This Method lets a player with playerName play the SlotMachine with a specific stake.
     * The player can only play if {@link #canPlayAgain} for this player returns true.
     * @param player The Client that is trying to play the SlotMachine.
     * @param stake The stake of the player.
     * Has to be within the boundaries of {@link Settings#getSlotmachineMinStake()}
     * and {@link Settings#getSlotmachineMaxStake()}.
     */
    public void play(Client player, int stake) {
        Expected.notNull(player);
        if (settings.getSlotmachineActivated() && canPlayAgain(player.getName())) {
            try {
                int minStake = settings.getSlotmachineMinStake();
                int maxStake = settings.getSlotmachineMaxStake();
                Expected.between(stake, minStake, maxStake);
                ArrayList<Integer> slots = getRandomSlots();
                Expected.listNotEmpty(Collections.singletonList(slots));
                int winning = calculateWinning(stake, slots);
                sendWinningMessage(player.getDisplayName(), slots, stake, winning);

                lastSlots.put(player.getName(), LocalDateTime.now());
                player.addMoney(winning - stake);
            } catch (IllegalArgumentException ignore) {
            }
            return;
        }
    }

    private ArrayList<Integer> getRandomSlots() {
        ArrayList<Integer> slots = new ArrayList<>();
        for (int i = 0; i < slotImgs.size(); i++) {
            slots.add((int) (Math.random() * slotImgs.size()));
        }
        return slots;
    }

    private void sendWinningMessage(String playerName, ArrayList<Integer> slots, int stake, int winning) {
        String slotOut = slotImgs.get(slots.get(0)).name + " | " + slotImgs.get(slots.get(1)).name + " | "
                + slotImgs.get(slots.get(2)).name;
        if (winning == 0) {
            utility.sendMessage(playerName + " bekommt " + slotOut + " und verliert " + (-(winning - stake)) + ".");
        } else {
            utility.sendMessage(playerName + " bekommt " + slotOut + " und gewinnt " + (winning));
        }
    }

    private int calculateWinning(int stake, ArrayList<Integer> slots) {
        if (slotsAllPositiveEqual(slots))
            return stake * settings.getSlotmachineThreeSameGood();
        else if (slotsAllPositive(slots))
            return stake * settings.getSlotmachineThreeGood();
        else if (slotsTwoTogetherPositive(slots))
            return stake * settings.getSlotmachineTwoGood();
        else if (slotsAllEqual(slots))
            return stake * settings.getSlotmachineThreeSameBad();
        return 0;
    }


    private boolean canPlayAgain(String client) {
        return !lastSlots.containsKey(client)
                || ChronoUnit.MINUTES.between(LocalDateTime.now(), lastSlots.get(client))
                        >= settings.getSlotMachineCoolDownMinutes();
    }

    private boolean slotsAllEqual(List<Integer> slots) {
        return (slots.get(0).equals(slots.get(1)) && slots.get(1).equals(slots.get(2)));
    }

    private boolean slotsTwoTogetherPositive(List<Integer> slots) {
        return (slotImgs.get(slots.get(0)).winning && slotImgs.get(slots.get(1)).winning ||
                slotImgs.get(slots.get(1)).winning && slotImgs.get(slots.get(2)).winning);
    }

    private boolean slotsAllPositive(List<Integer> slots) {
        return (slotImgs.get(slots.get(0)).winning &&
                slotImgs.get(slots.get(1)).winning &&
                slotImgs.get(slots.get(2)).winning);
    }

    private boolean slotsAllPositiveEqual(List<Integer> slots) {
        return (slots.get(0).equals(slots.get(1)) &&
                slots.get(1).equals(slots.get(2)) &&
                slotImgs.get(slots.get(0)).winning &&
                slotImgs.get(slots.get(1)).winning &&
                slotImgs.get(slots.get(2)).winning);
    }

}

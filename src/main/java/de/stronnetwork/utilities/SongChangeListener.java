package de.stronnetwork.utilities;

import de.stronnetwork.Player;

public interface SongChangeListener {
    void onSongNameUpdate(Player.UpdateReason reason, Object newValue);
}

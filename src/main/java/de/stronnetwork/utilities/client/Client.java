package de.stronnetwork.utilities.client;

/**
 * An abstract Client Class defining important Methods a Client needs. Use this for all use-cases of clients.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 2.0
 * @since 0.1
 */
public abstract class Client {

    /**
     * @return The wealth of this client.
     */
    public abstract int getMoney();

    /**
     * Adds the specified amount of Money to the total wealth.
     * @param addedMoney The money to add to this client.
     */
    public abstract void addMoney(int addedMoney);

    /**
     * This is the unique name of a client. No other client may have the same name.
     * @return The unique name of this client.
     */
    public abstract String getName();

    /**
     * This is the display name of a client which can differ from the unique name. Multiple clients can have this name.
     * If the unique name is needed use {@link #getName()}.
     * @return The display name.
     */
    public abstract String getDisplayName();

    /**
     * @return True if the client is found in the database.
     */
    public abstract boolean clientExistsInDatabase();

    /**
     * Adds a client if it doesn't exist.
     */
    public abstract void addClientToDatabase();
}

package de.stronnetwork.utilities.client;

import de.stronnetwork.Database;
import de.stronnetwork.utilities.Expected;

/**
 * An simple Client using the name as identifier, display name and real name.
 * Will not reflect any display name the user uses.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.6.0
 */
public class SimpleClient extends Client {

    private final String name;
    private final Database database;

    public SimpleClient(String name) {
        Expected.notNull(name);
        this.name = name;
        this.database = Database.getDatabase();
    }

    @Override
    public int getMoney() {
        return database.getMoney(name);
    }

    @Override
    public void addMoney(int addedMoney) {
        Expected.notNull(addedMoney);
        database.addMoney(name, addedMoney);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return name;
    }

    @Override
    public boolean clientExistsInDatabase() {
        return database.clientExists(name);
    }

    @Override
    public void addClientToDatabase() {
        database.addClient(name);
    }
}

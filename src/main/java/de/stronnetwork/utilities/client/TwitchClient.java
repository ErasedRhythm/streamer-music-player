package de.stronnetwork.utilities.client;

import com.gikk.twirk.types.users.TwitchUser;
import de.stronnetwork.Database;
import de.stronnetwork.utilities.Expected;

/**
 * A Client for the usage with the {@link TwitchUser} of {@link com.gikk.twirk.Twirk}.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.6.0
 */
public class TwitchClient extends Client {

    private final TwitchUser user;

    public TwitchClient(TwitchUser user) {
        Expected.notNull(user);
        this.user = user;
    }

    @Override
    public int getMoney() {
        return Database.getDatabase().getMoney(user.getUserName());
    }

    /**
     * @param addedMoney The money to add to this client. May be negative.
     */
    @Override
    public void addMoney(int addedMoney) {
        Expected.notNull(addedMoney);
        Database.getDatabase().addMoney(user.getUserName(), addedMoney);
    }

    @Override
    public String getName() {
        return user.getUserName();
    }

    @Override
    public String getDisplayName() {
        return user.getDisplayName();
    }

    @Override
    public boolean clientExistsInDatabase() {
        return Database.getDatabase().clientExists(user.getUserName());
    }

    @Override
    public void addClientToDatabase() {
        Database.getDatabase().addClient(user.getUserName());
    }

    public boolean isOwner() {
        return user.isOwner();
    }
}

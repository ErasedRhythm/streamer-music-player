package de.stronnetwork.utilities.playlist;

import de.stronnetwork.utilities.playlist.song.Song;
import de.stronnetwork.utilities.playlist.song.SongCandidate;

import java.util.ArrayList;

public class Playlist {
    private final ArrayList<Song> playlist;
    private final String name;

    private int currentPlaylistIndex = -1;

    public Playlist(String name, ArrayList<Song> playlist) {
        this.name = name;
        this.playlist = playlist;
    }

    public Song getCurrentSong() {
        return playlist.get(currentPlaylistIndex);
    }

    public Song nextSong() {
        if (playlist.isEmpty()) return null;
        int nextIndex = currentPlaylistIndex + 1;
        if (nextIndex >= playlist.size()) nextIndex = 0;
        return setCurrent(nextIndex);
    }

    public Song randomSong() {
        int currentValue = 0;
        ArrayList<SongCandidate> candidateList = new ArrayList<>();
        for (Song song : playlist) {
            int likes = 9 - song.getVotes();
            if (likes <= 0)
                likes = 1;
            int nextValue = currentValue + likes;
            candidateList.add(new SongCandidate(song, currentValue, nextValue));
            currentValue = nextValue + 1;
        }
        currentValue = currentValue - 1;

        int nextSongVal = (int) (Math.random() * currentValue);

        for (SongCandidate songCandidate : candidateList) {
            if (songCandidate.isInBound(nextSongVal)) {
                Song nextSong = songCandidate.getSong();
                setCurrent(getSongIndex(nextSong));
                return nextSong;
            }
        }
        return null;
    }

    private int getSongIndex(Song nextSong) {
        return playlist.indexOf(nextSong);
    }

    public ArrayList<Song> getAsArrayList() {
        return playlist;
    }

    public void addNextSong(Song song) {
        playlist.add(currentPlaylistIndex + 1, song);
    }

    public void addSong(Song song) {
        playlist.add(song);
    }

    public String getName() {
        return name;
    }

    public Song setCurrent(int selectedIndex) {
        if (currentPlaylistIndex == -1) currentPlaylistIndex = 0;
        playlist.get(currentPlaylistIndex).setPlaying(false);
        if (selectedIndex >= 0 && selectedIndex < playlist.size()) {
            currentPlaylistIndex = selectedIndex;
            playlist.get(selectedIndex).setPlaying(true);
            return playlist.get(selectedIndex);
        } return null;
    }

    public void removeSong(Song song) {
        playlist.remove(song);
    }

    public boolean isEmpty() {
        return playlist.isEmpty();
    }
}

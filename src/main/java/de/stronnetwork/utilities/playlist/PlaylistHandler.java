package de.stronnetwork.utilities.playlist;

import de.stronnetwork.utilities.Expected;
import de.stronnetwork.utilities.playlist.song.Song;

import java.util.*;

/**
 * A Song Library for a Music Player.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.6
 */

public class PlaylistHandler {
    private Playlist playlist;
    private ArrayDeque<Song> songQueue = new ArrayDeque<>();
    private boolean randomSong;
    private PlaylistReader reader = new PlaylistReader();

    /**
     * Constructor of {@link PlaylistHandler}.
     *
     * @param folderList A list containing all Folder Locations *.mp3 Files to add to the Library.
     */

    public PlaylistHandler() {
        playlist = reader.getPlaylist();
    }

    /**
     * This adds a {@link Song} to the last position of the queue.
     * If the queue is empty the added Song will be returned by {@link #getNextSong()} next.
     *
     * @param song The Song to add to the queue.
     *             TODO Entscheiden, wie es im GUI dargestellt werden sollte.
     */
    public void addToQueue(Song song) {
        Expected.notNull(song);
        songQueue.add(song);
    }

    /**
     * Returns the next Song in the queue. If the queue is empty a random Song of the Library is used. TODO change JavDoc
     * @return The next {@link Song} in the Library.
     * @throws NullPointerException If no Song is found.
     */
    public Song getNextSong() {
        Song nextSong;
        if (!songQueue.isEmpty()) nextSong = songQueue.pop();
        else if (randomSong) nextSong = playlist.randomSong();
        else nextSong = playlist.nextSong();
        Expected.notNull(nextSong, "Next Song not Found.");
        return nextSong;
    }

    /**
     * @return Returns the last Song {@link #getNextSong()} returned. May be null.
     */
    public Song getCurrentSong() {
        return playlist.getCurrentSong();
    }

    public ArrayList<Song> getPlaylist() {
        return playlist.getAsArrayList();
    }

    private void saveCurrentPlaylist() {
        reader.addPlaylist(getCurrentPlaylistName(), playlist.getAsArrayList());
    }

    public void addNextSong(Song nextSong) {
        playlist.addNextSong(nextSong);
    }

    public void setRandomSong(boolean setRandom) {
        randomSong = setRandom;
    }

    public String getCurrentPlaylistName() {
        return playlist.getName();
    }

    public void addSongToCurrentPlaylist(Song song) {
        playlist.addSong(song);
        saveCurrentPlaylist();
    }

    public Song getAndSetAsCurrent(int selectedIndex) {
        return playlist.setCurrent(selectedIndex);
    }

    public void removeSong(Song song) {
        playlist.removeSong(song);
        saveCurrentPlaylist();
    }

    public boolean isEmpty() {
        return playlist.isEmpty();
    }
}

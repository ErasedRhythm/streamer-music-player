package de.stronnetwork.utilities.playlist;

import com.github.kiulian.downloader.YoutubeException;
import com.mpatric.mp3agic.InvalidDataException;
import de.stronnetwork.utilities.playlist.song.FileSong;
import de.stronnetwork.utilities.playlist.song.Song;
import de.stronnetwork.utilities.playlist.song.YouTubeSong;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.*;

public class PlaylistReader {

    private final String fileName = "playlist.json";
    private boolean fileWritingError;
    private boolean fileReadingError;
    private JSONArray rootArray;

    public PlaylistReader() {
        File playlistFile = new File(fileName);
        if (!playlistFile.exists()) {
            createDefaultFile();
        } else if (!playlistFile.canRead()) {
            System.err.println(
                    "Config file is not readable! Starting with default settings. Will not update saved Settings till the error is solved.");
            fileReadingError = true;
            return;
        } else if (!playlistFile.canWrite()) {
            System.err.println("Config file not writable! Will not update saved Settings till the error is solved.");
            fileWritingError = true;
        }
        readFile(playlistFile);
    }

    private void createDefaultFile() {
        rootArray = new JSONArray();
        JSONObject defaultPlaylist = new JSONObject();
        defaultPlaylist.put("name", "default");
        defaultPlaylist.put("playlist", new JSONArray());
        rootArray.put(defaultPlaylist);
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(rootArray.toString(2));
        } catch (IOException e) {
            e.printStackTrace();
            fileWritingError = true;
        }
    }

    private void readFile(File playlistFile) {
        try {
            rootArray = new JSONArray(new JSONTokener(new FileReader(playlistFile)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addPlaylist(String name, ArrayList<Song> playlist) {
        boolean replaced = false;
        int i = 0;
        for (Object o : rootArray) {
            JSONObject playlistObject = (JSONObject) o;
            if (playlistObject != null && playlistObject.getString("name").equals(name)) {
                JSONArray playlistArray = new JSONArray();
                for (Song song: playlist) {
                    addSongToPlayListArray(playlistArray, song);
                }
                playlistObject.put("playlist", playlistArray);
                rootArray.put(i, playlistObject);
                replaced = true;
            }
            i++;
        }
        if (!replaced) {
            JSONObject playlistObject = new JSONObject();
            JSONArray playlistArray = new JSONArray();
            for (Song song: playlist) {
                addSongToPlayListArray(playlistArray, song);
            }
            playlistObject.put("playlist", playlistArray);
            rootArray.put(playlistObject);
        }
        saveFile();
    }

    private void addSongToPlayListArray(JSONArray playlistArray, Song song) {
        JSONObject songObject = new JSONObject();
        if (song instanceof FileSong)
            songObject.put("type", "file");
        else if (song instanceof YouTubeSong)
            songObject.put("type", "youtube");
        songObject.put("path", song.getPath());
        songObject.put("artist", song.getArtist());
        songObject.put("title", song.getTitle());
        songObject.put("album", song.getAlbum());
        playlistArray.put(songObject);
    }

    private void saveFile() {
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(rootArray.toString(2));
        } catch (IOException e) {
            e.printStackTrace();
            fileWritingError = true;
        }
    }

    public ArrayList<Song> getPlaylist(String playlistName) {
        JSONObject playlistObject = findPlaylist(playlistName);
        if (playlistObject == null) return new ArrayList<>();
        return readPlaylistObject(playlistObject);
    }

    private ArrayList<Song> readPlaylistObject(JSONObject playlistObject) {
        ArrayList<Song> songList = new ArrayList<>();
        for (Object obj2:playlistObject.getJSONArray("playlist")) {
            JSONObject jsonSong = (JSONObject) obj2;
            try {
                Song song;
                if (jsonSong.getString("type").equals("file"))
                    song = new FileSong(new File(jsonSong.getString("path")));
                else if (jsonSong.getString("type").equals("youtube"))
                    song = new YouTubeSong(jsonSong.getString("path"));
                else continue;
                String title = jsonSong.getString("title");
                String artist = jsonSong.getString("artist");
                String album = jsonSong.getString("album");
                song.setInfoWithoutLoad(title, artist, album);
                songList.add(song);
            } catch (FileNotFoundException ignore) {
            } catch (YoutubeException | IOException | YouTubeSong.VideoToLongException | InvalidDataException e) {
                e.printStackTrace();
            }
        }
        return songList;
    }

    private JSONObject findPlaylist(String playlistName) {
        for (Object obj1: rootArray) {
            JSONObject playlist = (JSONObject) obj1;
            if (playlist.getString("name").equals(playlistName))
                return playlist;
        }
        return null;
    }

    public Playlist getPlaylist() {
        JSONObject playlistObject = findPlaylist("default");

        Playlist playlist = new Playlist("default", new ArrayList<>());
        if (playlistObject == null) return playlist;
        for (Object obj2:playlistObject.getJSONArray("playlist")) {
            JSONObject jsonSong = (JSONObject) obj2;
            try {
                Song song;
                if (jsonSong.getString("type").equals("file"))
                    song = new FileSong(new File(jsonSong.getString("path")));
                else if (jsonSong.getString("type").equals("youtube"))
                    song = new YouTubeSong(jsonSong.getString("path"));
                else continue;
                String title = jsonSong.getString("title");
                String artist = jsonSong.getString("artist");
                String album = jsonSong.getString("album");
                song.setInfoWithoutLoad(title, artist, album);
                playlist.addSong(song);
            } catch (FileNotFoundException ignore) {
            } catch (YoutubeException | IOException | YouTubeSong.VideoToLongException | InvalidDataException e) {
                e.printStackTrace();
            }
        }
        return playlist;
    }
}

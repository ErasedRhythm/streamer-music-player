package de.stronnetwork.utilities.playlist.song;

import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import de.stronnetwork.Database;
import de.stronnetwork.utilities.Expected;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * A Song Class for .mp3 Files accessible via {@link File}.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.5.0
 */
public class FileSong extends Song {

    private final File songFile;
    private Mp3File mp3File;

    /**
     * Song Constructor for local Files.
     * @param songFile The File to reference this Object to.
     * @throws FileNotFoundException If the given File does not Exist.
     */
    public FileSong(File songFile) throws FileNotFoundException, InvalidDataException {
        Expected.fileExists(songFile);
        this.songFile = songFile;
        checkIfMp3();
        Uid = songFile.getAbsolutePath();
        Database.getDatabase().addSong(Uid);
    }

    private void checkIfMp3() throws InvalidDataException {
        try {
            new Mp3File(songFile.getAbsolutePath());
        } catch (IOException | UnsupportedTagException e) {
            e.printStackTrace();
        }
    }

    private Mp3File getSongMp3() throws InvalidDataException, IOException, UnsupportedTagException {
        if (mp3File != null) return mp3File;
        mp3File = new Mp3File(songFile.getAbsolutePath());
        return mp3File;
    }

    @Override
    public String getPath() {
        return songFile.toPath().toString();
    }

    @Override
    public String getURI() {
        return songFile.toURI().toString();
    }

    @Override
    protected void loadArtist() {
        try {
            Mp3File current = getSongMp3();
            artist = current.getId3v2Tag().getArtist();
        } catch (IOException | UnsupportedTagException | InvalidDataException e) {
            e.printStackTrace();
            artist = "";
        }
    }

    @Override
    protected void loadAlbum() {
        try {
            Mp3File current = getSongMp3();
            album = current.getId3v2Tag().getAlbum();
        } catch (IOException | UnsupportedTagException | InvalidDataException e) {
            e.printStackTrace();
            album = "";
        }
    }

    @Override
    protected void loadTitle() {
        try {
            Mp3File current = getSongMp3();
            title = current.getId3v2Tag().getTitle();
        } catch (IOException | UnsupportedTagException | InvalidDataException e) {
            e.printStackTrace();
            title = "Unknown";
        }
    }

    @Override
    public byte[] getAlbumImg() {
        try {
            Mp3File current = getSongMp3();
            return current.getId3v2Tag().getAlbumImage();
        } catch (IOException | UnsupportedTagException | InvalidDataException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isVideo() {
        return false;
    }
}

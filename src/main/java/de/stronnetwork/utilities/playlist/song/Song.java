package de.stronnetwork.utilities.playlist.song;

import de.stronnetwork.Database;

import java.util.ArrayList;
import java.util.Objects;

/**
 * An abstract Song Class implementing every important method for Songs.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.4
 */
public abstract class Song {

    private boolean isPlaying;
    protected String Uid;

    public boolean isLoaded = false;
    protected String title;
    protected String album;
    protected String artist;

    private ArrayList<String> currentVoters = new ArrayList<>();

    protected Song() {}

    /**
     * The path to the song File. URL if {@link #isVideo()} is true, local path otherwise.
     * @return The file path.
     */
    abstract public String getPath();

    /**
     *
     */
    abstract public String getURI();

    /**
     * @return The Title of the Song.
     */
    public String getTitle() {
        if (!isLoaded) load();
        return title;

    }

    public String getAlbum() {
        if (!isLoaded) load();
        return album;
    }

    public void setInfoWithoutLoad(String title, String artist, String album) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        isLoaded = true;
    }

    public void load() {
        if (isLoaded) return;
        loadTitle();
        loadAlbum();
        loadArtist();
        isLoaded = true;
    }

    abstract protected void loadArtist();

    abstract protected void loadAlbum();

    abstract protected void loadTitle();

    /**
     * @return Album Image or Thumbnail. May be null.
     */
    abstract public byte[] getAlbumImg();

    /**
     * @return True if the Object was constructed with a YouTube Video and returns a YouTube Link as Path.
     */
    abstract public boolean isVideo();

    /**
     * @return Total like and dislikes all Time of this Song.
     */
    public int getVotes() {
        return Database.getDatabase().getSongRating(Uid);
    }

    /**
     * Adds the voters Name as voter and adds or subtracts 1 to the total rating of this song.
     * If the voter does already exist no change is done.
     * @param voterName The voters unique Name.
     * @param isPositiveVote Determines weather the total rating is adding or subtracting 1 to/from the total rating.
     * @return True if Voter did not vote before.
     */
    public boolean addVoter(String voterName, boolean isPositiveVote) {
        if (currentVoters.contains(voterName)) return false;
        currentVoters.add(voterName);
        if (isPositiveVote)
            Database.getDatabase().updateSongRating(Uid, getVotes() + 1);
        else Database.getDatabase().updateSongRating(Uid, getVotes() - 1);
        return true;
    }

    /**
     * Resets the voter List. Enables everyone to vote for this song again.
     */
    public void resetVoters() {
        currentVoters = new ArrayList<>();
    }

    public String getArtist() {
        if (!isLoaded) load();
        return artist;
    }

    @SuppressWarnings("unused")
    public String getPlaying() {
        if (isPlaying)
            return "\u25ba";
        return "";
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return Uid.equals(song.Uid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Uid);
    }
}

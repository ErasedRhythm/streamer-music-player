package de.stronnetwork.utilities.playlist.song;

import de.stronnetwork.utilities.Expected;

/**
 * An song optimised to be part of a random selection with weighting.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.1
 */
public class SongCandidate {

    private Song song;
    private Integer minValue;
    private Integer maxValue;

    /**
     * Constructs a new SongCandidate.
     * @param song The Song of this candidate.
     * @param minValue The start Value of the Candidate.
     * @param maxValue The end Value of the Candidate.
     */
    public SongCandidate(Song song, Integer minValue, Integer maxValue) {
        this.song = song;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    /**
     * Returns true if the value is between or equal the minimum Value or maximum Value given in the Constructor. False otherwise.
     * @param value the value to compare to.
     * @return True if the value is in Bounds. False otherwise.
     */
    public boolean isInBound(Integer value) {
        Expected.notNull(value);
        return (minValue <= value && value <= maxValue);
    }

    /**
     * @return Returns a new Song Object based on the Location given to the Constructor.
     */
    public Song getSong() {
        return song;
    }
}
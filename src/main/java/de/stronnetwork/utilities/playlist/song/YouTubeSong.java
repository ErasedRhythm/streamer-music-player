package de.stronnetwork.utilities.playlist.song;

import com.github.kiulian.downloader.YoutubeDownloader;
import com.github.kiulian.downloader.YoutubeException;
import com.github.kiulian.downloader.model.YoutubeVideo;
import com.github.kiulian.downloader.model.formats.AudioVideoFormat;
import com.github.kiulian.downloader.model.formats.Format;
import de.stronnetwork.Database;
import de.stronnetwork.Settings;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class YouTubeSong extends Song {

    public class VideoToLongException extends Exception{}

    public final YoutubeVideo video;
    private YoutubeDownloader downloader;
    private final int MAX_VIDEO_LENGTH = 600;

    /**
     * Song Constructor for YouTube Videos.
     * @param url The HTTPS Link leading to the video.
     */
    public YouTubeSong(String url) throws IOException, YoutubeException, VideoToLongException {
        iniYouTubeDownloader();
        video = downloader.getVideo(convertToYoutubeVideoID(url));
        if (video.details().lengthSeconds() > MAX_VIDEO_LENGTH) throw new VideoToLongException();
        Uid = video.details().videoId();
        Database.getDatabase().addSong(Uid);
    }

    private void iniYouTubeDownloader() {
        // init downloader
        downloader = new YoutubeDownloader();

        // or just extend functionality via existing API
        // cipher features
        downloader.addCipherFunctionPattern(2, "\\b([a-zA-Z0-9$]{2})\\s*=\\s*function\\(\\s*a\\s*\\)\\s*\\{\\s*a\\s*=\\s*a\\.split\\(\\s*\"\"\\s*\\)");
        // extractor features
        downloader.setParserRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
        downloader.setParserRetryOnFailure(1);
    }

    private String convertToYoutubeVideoID(String url) {
        if (url.startsWith("https://www.youtube.com/watch?v=") ) {
            url = url.replace("https://www.youtube.com/watch?v=", "");
        } else if (url.startsWith("https://youtu.be/")) {
            url = url.replace("https://youtu.be/", "");
        }
        return url.split("&", 2)[0];
    }

    @Override
    public String getPath() {
        return video.details().videoId();
    }

    @Override
    public String getURI() {
        List<AudioVideoFormat> audioVideoFormats = new LinkedList<>();
        for (int i = 0; i < video.formats().size(); i++) {
            Format format = video.formats().get(i);
            if (format instanceof AudioVideoFormat)
                audioVideoFormats.add((AudioVideoFormat) format);
        }
        Format formatByItag;
        formatByItag = video.findFormatByItag(18);
        if (formatByItag == null) {
            if (Settings.getSettings().isDebug()) {
                System.err.println("Video not found. Possible IDs:");
                audioVideoFormats.forEach(it -> System.err.println("  * " + it.itag().id()));
            }
            return null;
        }
        return formatByItag.url();
    }

    @Override
    protected void loadArtist() {
        artist = video.details().author();
    }

    @Override
    protected void loadAlbum() {
        album = "";
    }

    @Override
    protected void loadTitle() {
        title = video.details().title();
    }

    @Override
    public byte[] getAlbumImg() {
        try {
            List<String> tumbnials = video.details().thumbnails();
            String imgURL = tumbnials.get(tumbnials.size() - 2).split("[?]")[0];
            URL url = new URL(imgURL);
            if (Settings.getSettings().isDebug())
                System.out.println("Video Thumbnail: " + imgURL);
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            InputStream inputStream = url.openStream();
            int n;
            byte [] buffer = new byte[ 1024 ];
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
            return output.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isVideo() {
        return true;
    }
}

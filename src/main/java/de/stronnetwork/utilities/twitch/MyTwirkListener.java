package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.events.TwirkListener;

public interface MyTwirkListener extends TwirkListener {
    String getHelpMessage();

}

package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.types.twitchMessage.TwitchMessage;
import com.gikk.twirk.types.users.TwitchUser;

import java.util.List;

public class TwitchHelpCommandListener implements MyTwirkListener {

    private List<MyTwirkListener> listenerList;
    private TwitchUtility twitchUtility;

    public TwitchHelpCommandListener(TwitchUtility twitchUtility, List<MyTwirkListener> listenerList) {
        this.twitchUtility = twitchUtility;
        this.listenerList = listenerList;
    }

    @Override
    public String getHelpMessage() {
        return null;
    }

    @Override
    public void onPrivMsg(TwitchUser sender, TwitchMessage message) {
        String[] command = message.getContent().split(" ");
        if (command[0].equals("!help")) {
            String helpMessages = "";
            for (MyTwirkListener listener: listenerList) {
                String hMessage = listener.getHelpMessage();
                if (hMessage != null && !hMessage.isEmpty()) {
                    helpMessages += hMessage + " || ";
                }
            }
            twitchUtility.sendMessage(helpMessages);
        }
    }
}

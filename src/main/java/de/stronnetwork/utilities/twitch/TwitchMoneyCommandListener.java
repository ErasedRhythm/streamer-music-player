package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.Twirk;
import com.gikk.twirk.types.twitchMessage.TwitchMessage;
import com.gikk.twirk.types.users.TwitchUser;
import de.stronnetwork.Database;
import de.stronnetwork.Settings;
import de.stronnetwork.utilities.client.Client;
import de.stronnetwork.utilities.SlotMachine;
import de.stronnetwork.utilities.client.SimpleClient;
import de.stronnetwork.utilities.client.TwitchClient;

import java.util.List;
import java.util.Map;

/**
 * This is a listener for {@link Twirk} which listens for any incoming messages, which uses Money directly.
 * Any other Listener can still make use of the Money of a Client.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.4.0
 */
public class TwitchMoneyCommandListener implements MyTwirkListener {

    private final TwitchUtility twitchUtility;
    private final Twirk twirk;
    private final Database database = Database.getDatabase();
    private final Settings settings = Settings.getSettings();
    private final SlotMachine slotMachine;

    public TwitchMoneyCommandListener(TwitchUtility twitchUtility, Twirk twirk) {
        this.twitchUtility = twitchUtility;
        this.twirk = twirk;
        slotMachine = new SlotMachine(twitchUtility);
    }

    @Override
    public String getHelpMessage() {
        if (!settings.getMoneyActivated()) return null;
        String msg = "";
        if (!settings.getMoneyCheckCommands().isEmpty())
            msg += settings.getMoneyCheckCommands().get(0) + " - How is my wealth?";
        msg += " || !give <User> <Amount> - show your love with another person!";
        msg += " || !top - the top 1% of all?";
        if (settings.getSlotmachineActivated())
            msg += " || !slots <Stake> - Got some luck?";
        return msg;
    }

    @Override
    public void onPrivMsg(TwitchUser sender, TwitchMessage message) {
        if (!settings.getMoneyActivated()) return;
        TwitchClient sendingClient = new TwitchClient(sender);
        String[] command = message.getContent().split(" ");
        if (settings.getMoneyCheckCommands().contains(command[0])) {
            int money = database.getMoney(sendingClient.getName());
            twitchUtility.sendMessage(sendingClient.getDisplayName() + " has " + money + " " + settings.getMoneyName());
        } else switch (command[0]) {
            case "!add":
                if (command[1] != null && command[2] != null) {
                    try {
                        addMoney(sendingClient, new SimpleClient(command[1]), Integer.parseInt(command[2]));
                    } catch (NumberFormatException ex) {
                        ex.getMessage();
                    }
                }
                break;
            case "!give":
                try {
                    give(sendingClient, new SimpleClient(command[1]), Integer.parseInt(command[2]));
                } catch (NumberFormatException ignore) {}
                break;
            case "!top":
                printTopTen();
                break;
            case "!slots":
                if (command[1] != null)
                    try {
                        int stake = Integer.parseInt(command[1]);
                        slotMachine.play(sendingClient, stake);
                    } catch (NumberFormatException ignore) {}
                break;
            default:
                break;
        }
    }

    private void give(Client giver, Client receiver, int givenMoney) {
        if (receiver != null && receiver.clientExistsInDatabase() && givenMoney > 0 && givenMoney <= giver.getMoney()) {
            giver.addMoney(-givenMoney);
            receiver.addMoney(givenMoney);
            twitchUtility.sendMessage(giver + " shows " + receiver + " some love <3");
        }
    }

    private void addMoney(TwitchClient sender, Client benefitingClient, int amount) {
        if (sender.isOwner()) {
            try {
                if (benefitingClient.clientExistsInDatabase())
                benefitingClient.addMoney(amount);
            } catch (NumberFormatException ex) {
                ex.getCause();
            }
        }
    }

    private void printTopTen() {
        List<Map.Entry<Integer, SimpleClient>> topMoneyUsers = database.getTopMoneyClients();
        if (topMoneyUsers == null)
            return;
        StringBuilder msg = new StringBuilder();
        int position = 1;

        for (Map.Entry<Integer, SimpleClient> entry : topMoneyUsers) {
            //noinspection StringConcatenationInsideStringBufferAppend
            msg.append(position + ". " + entry.getValue().getDisplayName() + "(" + entry.getKey() + "), ");
            position++;
        }

        twitchUtility.sendMessage(msg.toString());
    }

    @Override
    public void onJoin(String joinedNick) {
        new SimpleClient(joinedNick).addClientToDatabase();
    }

    @Override
    public void onConnect() {
        for (String user: twirk.getUsersOnline()) {
            new SimpleClient(user).addClientToDatabase();
        }
    }
}

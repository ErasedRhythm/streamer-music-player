package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.types.twitchMessage.TwitchMessage;
import com.gikk.twirk.types.users.TwitchUser;
import de.stronnetwork.Settings;

public class TwitchMultiCommandListener implements MyTwirkListener {

    private TwitchUtility twitchUtility;

    public TwitchMultiCommandListener(TwitchUtility twitchUtility) {
        this.twitchUtility = twitchUtility;
    }

    @Override
    public void onPrivMsg(TwitchUser sender, TwitchMessage message) {
        String[] command = message.getContent().split(" ");
        if (command[0].equals("!multi")) {
            Settings settings = Settings.getSettings();
            String url = "https://multistre.am/" + settings.getAuthChannel().replace("#", "") + "/";
            int streamers = 0;
            if (!settings.getMultiFirstStreamer().isEmpty()) {
                url += settings.getMultiFirstStreamer() + "/";
                streamers++;
            }
            if (!settings.getMultiSecondStreamer().isEmpty()) {
                url += settings.getMultiSecondStreamer() + "/";
                streamers++;
            }
            if (!settings.getMultiThirdStreamer().isEmpty()) {
                url += settings.getMultiThirdStreamer() + "/";
                streamers++;
            }
            switch (streamers) {
                case 1:
                    url += "layout4/";
                    break;
                case 2:
                    url += "layout6/";
                    break;
                case 3:
                    url += "layout10/";
                    break;
                default:
                    break;
            }
            if (streamers > 0)
                twitchUtility.sendMessage("Watch us all together on: " + url);
        }
    }

    @Override
    public String getHelpMessage() {
        Settings settings = Settings.getSettings();
        if (!(settings.getMultiFirstStreamer().isEmpty() &&
            settings.getMultiSecondStreamer().isEmpty() &&
            settings.getMultiThirdStreamer().isEmpty()))
            twitchUtility.sendMessage("!multi - Everyone streaming with me!");
        return null;
    }
}

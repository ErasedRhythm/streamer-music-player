package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.types.twitchMessage.TwitchMessage;
import com.gikk.twirk.types.users.TwitchUser;
import de.stronnetwork.Player;
import de.stronnetwork.Settings;

/**
 * This Listener listens for command that are related to the {@link Player}.
 *
 * @author ErasedRhythm
 * @author https://gitlab.com/ErasedRhythm
 * @version 1.0
 * @since 0.1
 */
public class TwitchPlayerCommandListener implements MyTwirkListener {

    private final Settings settings = Settings.getSettings();
    private TwitchUtility util;

    /**
     * The Constructor of this Class. The TwitchUtility is needed to send messages back to the chat.
     * @param twitchUtility Needed to send messages.
     */
    public TwitchPlayerCommandListener(TwitchUtility twitchUtility) {
        this.util = twitchUtility;
    }

    @Override
    public void onPrivMsg(TwitchUser sender, TwitchMessage message) {
        if (!settings.getPlayerTwitchVotingEnabled()) return;
        Player player = Player.getInstance();
        String[] command = message.getContent().split(" ");

        if (settings.getPlayerVotingCommandsGood().contains(command[0])) {
            if (player.getCurrentSong().addVoter(sender.getUserName(), true))
                util.sendMessage(sender.getUserName() + " likes the Song! B-)");
            else util.sendMessage("You can only vote once per Song!");
            return;
        } else if (settings.getPlayerVotingCommandsBad().contains(command[0])) {
            if (player.getCurrentSong().addVoter(sender.getUserName(), false))
                util.sendMessage(sender.getUserName() + " dislikes the Song! :-/");
            else util.sendMessage("You can only vote once per Song!");
            return;
        }

        if (command[0].equals("!next") && (sender.isMod() || sender.isOwner()))
            player.nextSong();

        else if (command[0].equals("!playing"))
            util.sendMessage("Now Playing: " + player.getCurrentSong().getTitle());

        else if ((command[0].equals("!volume") || command[0].equals("!v")) && (sender.isMod() || sender.isOwner())) {
            if (command[1] != null) {
                player.changeVolume(Integer.parseInt(command[1]));
            }
        }
    }

    @Override
    public String getHelpMessage() {
        String helpMessage = "";
        if (settings.getPlayerTwitchVotingEnabled()) {
            String votingGood = settings.getPlayerVotingCommandsGood().get(0);
            if (!(votingGood.isEmpty()))
                helpMessage = helpMessage + votingGood + " - I like the Song! || ";

            String votingBad = settings.getPlayerVotingCommandsBad().get(0);
            if (!(votingBad.isEmpty()))
                helpMessage = helpMessage + votingBad + " - I dislike the Song! || ";
        }
        helpMessage = helpMessage + "!playing - What's the song?";

        return helpMessage;
    }
}

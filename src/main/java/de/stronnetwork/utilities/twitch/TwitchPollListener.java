package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.types.twitchMessage.TwitchMessage;
import com.gikk.twirk.types.users.TwitchUser;
import javafx.application.Platform;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TwitchPollListener implements MyTwirkListener {
    private final TwitchUtility twitchUtility;
    private boolean pollActive = false;
    private BarChart<?, ?> barChart;
    private HashSet<String> participatedUsers;

    private List<Answer> answerList;

    private static class Answer {
        String answer;
        int votes;
        public Answer(String answer, int votes) {
            this.answer = answer;
            this.votes = votes;
        }

        public void addVote() {
            votes++;
        }
    }


    public TwitchPollListener(TwitchUtility twitchUtility) {
        this.twitchUtility = twitchUtility;
    }

    @Override
    public void onPrivMsg(TwitchUser sender, TwitchMessage message) {
        String[] command = message.getContent().split(" ");
        if (!pollActive) return;
        try {
            int vote = Integer.parseInt(command[0]);
            if (answerList.size() >= vote && vote > 0 && !participatedUsers.contains(sender.getUserName())){
                answerList.get(vote - 1).addVote();
                redrawPoll();
                participatedUsers.add(sender.getUserName());
            }
        } catch (NumberFormatException e) {/*Do Nothing*/}
    }

    private void redrawPoll() {
        XYChart.Series series = new XYChart.Series();
        int totalVotes = 0;
        for (Answer answer: answerList) {
            totalVotes += answer.votes;
        }
        for (Answer answer: answerList) {
            series.getData().add((new XYChart.Data(answer.answer, (answer.votes * 100) / totalVotes)));
        }
        Platform.runLater(() -> {
            barChart.getData().clear();
            barChart.layout();
            barChart.getData().addAll(series);
        });
    }

    @Override
    public String getHelpMessage() {
        return null;
    }

    public void startPoll(BarChart<?, ?> barChart, String question, ArrayList<String> answers, int timerMinutes) {
        this.barChart = barChart;
        this.participatedUsers = new HashSet<>();
        barChart.setTitle(question);
        XYChart.Series series = new XYChart.Series();
        answerList = new ArrayList<>();
        String pollMsg1 = "Poll Started! Question: " + question;
        String pollMsg2 = "";
        int i = 1;
        for (String answer: answers) {
            series.getData().add((new XYChart.Data(answer, 100/answers.size())));
            answerList.add(new Answer(answer, 0));
            pollMsg2 += i + ". " + answer + " ";
            i++;
        }

        barChart.getData().clear();
        barChart.layout();
        barChart.getData().addAll(series);

        pollActive = true;

        twitchUtility.sendMessage(pollMsg1);
        twitchUtility.sendMessage(pollMsg2);

        createTimer(timerMinutes);
    }

    private void createTimer(int timerMinutes) {
        new Thread(()-> {
            try {
                TimeUnit.MINUTES.sleep(timerMinutes);
                pollActive = false;
                pollFinal();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void pollFinal() {
        int totalVotes = 0;
        int highestVote = 0;
        ArrayList<Answer> winners = new ArrayList<>();
        for (Answer answer: answerList) {
            totalVotes += answer.votes;
            if (answer.votes > highestVote) {
                highestVote = answer.votes;
                winners = new ArrayList<>();
                winners.add(answer);
            } else if (answer.votes == highestVote){
                winners.add(answer);
            }
        }
        printWinner(totalVotes, highestVote, winners);
    }

    private void printWinner(int totalVotes, int highestVote, ArrayList<Answer> winners) {
        if (winners.size() == 1) {
            twitchUtility.sendMessage("Answer: \"" + answerList.get(0).answer + "\" has won with " + highestVote + " Votes (" + totalVotes + " total)!");
        } else if (winners.size() > 1) {
            StringBuilder msg = new StringBuilder("It's a draw! ");
            for (int i = 0; i < winners.size(); i++) {
                if (i == 0) {
                    msg.append("\"").append(winners.get(i).answer).append("\"");
                } else msg.append(" & \"").append(winners.get(i).answer).append("\"");
            }
            msg.append(" got ").append(highestVote).append(" Votes (").append(totalVotes).append(" total)");
            twitchUtility.sendMessage(msg.toString());
        }
    }


}

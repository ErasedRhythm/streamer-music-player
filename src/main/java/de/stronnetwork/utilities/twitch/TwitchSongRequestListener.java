package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.types.twitchMessage.TwitchMessage;
import com.gikk.twirk.types.users.TwitchUser;
import com.github.kiulian.downloader.YoutubeException;
import de.stronnetwork.Player;
import de.stronnetwork.Settings;
import de.stronnetwork.utilities.playlist.song.Song;
import de.stronnetwork.utilities.playlist.song.YouTubeSong;

import java.io.IOException;

public class TwitchSongRequestListener implements MyTwirkListener {


    private TwitchUtility twitchUtility;
    private boolean songRequestActive;

    public TwitchSongRequestListener(TwitchUtility twitchUtility) {
        this.twitchUtility = twitchUtility;
        songRequestActive = Settings.getSettings().getPlayerSongRequestActive();
    }

    @Override
    public String getHelpMessage() {
        return "!sr <URL> - Let us listen to this song";
    }

    @Override
    public void onPrivMsg(TwitchUser sender, TwitchMessage message) {
        String[] command = message.getContent().split(" ");
        if (songRequestActive && command[0].equals("!sr") && command[1] != null) {
            try {
                Song song = new YouTubeSong(command[1]);
                Player.getInstance().addSongRequest(song);
                twitchUtility.sendMessage("\"" + song.getTitle() + "\"" + " was requested by " + sender.getDisplayName());

            } catch (YoutubeException | IOException e) {
                twitchUtility.sendMessage("Could not find the song, sorry.");
                System.out.println("[INFO] Try restarting your router to change your IP if this happens regularly!");
                e.printStackTrace();
            } catch (YouTubeSong.VideoToLongException e) {
                twitchUtility.sendMessage(sender.getDisplayName() + " Song is too long.");
            }
        }
    }

    private String convertToYoutubeVideoID(String[] command) {
        if (command[1].startsWith("https://www.youtube.com/watch?v=") ) {
            String songURL = command[1].replace("https://www.youtube.com/watch?v=", "");
            return songURL.split("&", 2)[0];
        } else if (command[1].startsWith("https://youtu.be/")) {
            return command[1].replace("https://youtu.be/", "");
        }
        return command[1];
    }
}

package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.Twirk;
import de.stronnetwork.Database;
import de.stronnetwork.Settings;

import java.util.concurrent.TimeUnit;

public class TwitchTimeRewarder extends Thread {

    private Twirk twirk;
    private Database database = Database.getDatabase();

    public TwitchTimeRewarder(Twirk twirk) {
        this.twirk = twirk;
        start();
    }

    @Override
    public void run() {
        int sleepTime = 1;
        while(!this.isInterrupted()) {
            try {
                TimeUnit.MINUTES.sleep(sleepTime);
                addMoneyToAllUsers(10);
                addTimeToAllUsers(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private void addTimeToAllUsers(int time) {
        for (String user:twirk.getUsersOnline()) {
            if (!(Settings.getSettings().getMoneyBannedClients().contains(user) || twirk.getNick().equals(user)))
                database.addTime(user, time);
        }
    }

    private void addMoneyToAllUsers(int money) {
        for (String user: twirk.getUsersOnline()) {
            if (!Settings.getSettings().getMoneyBannedClients().contains(user) && !user.equals(twirk.getNick()))
            database.addMoney(user, money);
        }
    }
}

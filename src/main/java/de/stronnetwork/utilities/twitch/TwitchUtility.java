package de.stronnetwork.utilities.twitch;

import com.gikk.twirk.Twirk;
import com.gikk.twirk.TwirkBuilder;
import com.gikk.twirk.events.TwirkListener;
import de.stronnetwork.Settings;
import javafx.scene.chart.BarChart;

import java.io.IOException;
import java.util.ArrayList;

public class TwitchUtility {

    private Settings settings;
    private Twirk twirk;
    private TwitchPollListener pollHandler;

    public TwitchUtility() {
        this.settings = Settings.getSettings();
        initTwirk();
    }

    private void initTwirk() {
        try {
            String nickname = settings.getAuthNickname();
            if (!nickname.startsWith("#"))
                nickname = "#" + nickname;
            this.twirk = new TwirkBuilder(settings.getAuthChannel().toLowerCase(), nickname, settings.getAuthKey()).setVerboseMode(settings.isDebug())
                    .build();
            initListeners();
            twirk.connect();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initListeners() {
        ArrayList<MyTwirkListener> listenerList = new ArrayList<>();
        listenerList.add(new TwitchPlayerCommandListener(this));
        listenerList.add(new TwitchMultiCommandListener(this));
        pollHandler = new TwitchPollListener(this);
        listenerList.add(pollHandler);
        listenerList.add(new TwitchSongRequestListener(this));
        listenerList.add(new TwitchHelpCommandListener(this, listenerList));
        listenerList.add(new TwitchMoneyCommandListener(this, twirk));

        for (MyTwirkListener listener: listenerList) {
            twirk.addIrcListener(listener);
        }

        new TwitchTimeRewarder(twirk);
    }

    public void sendMessage(String message) {
        if (message.length() <= 496)
            twirk.channelMessage("/me " + message);
        else {
            while (!message.isEmpty()) {
                twirk.channelMessage("/me " + message.substring(0, 496));
                message = message.substring(496);
            }
        }
    }

    public void startPoll(BarChart<?, ?> barChart, String question, ArrayList<String> answers, int timerMinutes) {
        pollHandler.startPoll(barChart, question, answers, timerMinutes);
    }


}
